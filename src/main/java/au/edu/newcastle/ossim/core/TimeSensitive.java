package au.edu.newcastle.ossim.core;

/**
 * User: Jake Woods
 * Date: 3/07/13
 *
 * Any class that wishes to operating within the time-step simulation must implement TimeSensitive
 * and it's members according to the below comments. A TimeSensitive class is able to work in step with other
 * TimeSensitive classes -- such as coordinating processes and IO.
 *
 * The order in which simulations are run is a closed loop of [step(), update()]. That is: step() is called first
 * followed by update(). The first moment of time in the simulation is time 0.
 */
public interface TimeSensitive {

    /**
     * Update is called once for every time-unit simulated within the system regardless of if the implementing class
     * has any work to do.
     */
    void update();

    /**
     * Step is called whenever the implementing class is expected to complete a single time-units worth of work.
     * It is not guaranteed that step will be called for every time-step in the simulation. For that you should use
     * update.
     */
    void step();

    /**
     * Resets can be triggered by the user. When this method is called the implementing class needs to reset
     * to it's default starting state equivalent to it's state at time-step 0.
     */
    void reset();
}
