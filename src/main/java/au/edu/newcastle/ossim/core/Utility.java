package au.edu.newcastle.ossim.core;

import java.util.Map;

/**
 * User: Jake Woods
 * Date: 3/07/13
 *
 * Any project of scope will have a class like this. Especially in a language as poor as Java. Essentially this
 * is a set of static methods used to circumvent the shortcomings of the Java standard libraries. Normally I would
 * use something like Guava or Apache Commons but as this is a project for students I need to steer clear of them.
 */
public class Utility {

    /**
     * Finds the largest key for a given map.
     *
     * @param map The map to search
     * @param startValue The value to start searching from.
     * @param <K> The key type of the map
     * @param <V> The value type of the map
     * @return The largest key in the map.
     */
    public static <K extends Comparable<? super K>, V> K getLargestKeyInMap(Map<K, V> map, K startValue) {
        K largest = startValue;
		for(K entry : map.keySet()) {
            if(largest == null) {
                largest = entry;
            } else if(entry.compareTo(largest) > 0) { // Checks for entry > largest
				largest = entry;
			}
		}
		return largest;
    }
}
