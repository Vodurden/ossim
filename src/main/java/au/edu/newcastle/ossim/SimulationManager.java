package au.edu.newcastle.ossim;

import au.edu.newcastle.ossim.cpu.execution.Instruction;
import au.edu.newcastle.ossim.cpu.execution.Process;
import au.edu.newcastle.ossim.cpu.scheduler.ProcessScheduler;
import au.edu.newcastle.ossim.io.device.IODevice;
import au.edu.newcastle.ossim.io.scheduler.IOScheduler;
import au.edu.newcastle.ossim.loader.ProcessDataSource;
import au.edu.newcastle.ossim.loader.ProcessDeserializer;
import au.edu.newcastle.ossim.paging.device.PagingDevice;
import au.edu.newcastle.ossim.paging.replacer.PageReplacer;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class SimulationManager {
	
	// GUI-Choosable stuff
	private List<Class<? extends ProcessScheduler>> m_schedulers = new ArrayList<Class<? extends ProcessScheduler>>();
	private List<Class<? extends PagingDevice>> m_pagingDevices = new ArrayList<Class<? extends PagingDevice>>();
	private List<Class<? extends IODevice>> m_ioDevices = new ArrayList<Class<? extends IODevice>>();
	private List<Class<? extends IOScheduler>> m_ioSchedulers = new ArrayList<Class<? extends IOScheduler>>();
    private List<Class<? extends PageReplacer>> m_pageReplacers = new ArrayList<Class<? extends PageReplacer>>();

	// GUI-Selected stuff
	private List<Class<? extends ProcessScheduler>> m_selectedSchedulers = new ArrayList<Class<? extends ProcessScheduler>>();
	private List<Class<? extends PagingDevice>> m_selectedPagingDevices = new ArrayList<Class<? extends PagingDevice>>();
	private List<Class<? extends IODevice>> m_selectedIODevices = new ArrayList<Class<? extends IODevice>>();
	private List<Class<? extends IOScheduler>> m_selectedIOSchedulers = new ArrayList<Class<? extends IOScheduler>>();
    private List<Class<? extends PageReplacer>> m_selectedPageReplacers = new ArrayList<Class<? extends PageReplacer>>();
	
	private ProcessDeserializer m_processDeserializer;
	
	public SimulationManager(ProcessDeserializer deserializer) {
		m_processDeserializer = deserializer;
	}
	
	public SimulationSet buildSimulationSet(ProcessDataSource source) {
		// We want to built one simulation for each combination of scheduler, devices and IO devices and add it to
		// the simulation set.
		SimulationSet set = new SimulationSet();
		for(Class<? extends ProcessScheduler> schedulerClass : m_selectedSchedulers) {
			for(Class<? extends PagingDevice> pagingDeviceClass : m_selectedPagingDevices) {
                for(Class<? extends PageReplacer> pageReplacerClass : m_selectedPageReplacers) {
                    for(Class<? extends IODevice> ioDeviceClass : m_selectedIODevices) {
                        for(Class<? extends IOScheduler> ioSchedulerClass : m_selectedIOSchedulers) {
                            ProcessScheduler scheduler = create(schedulerClass);
                            PageReplacer pageReplacer = create(pageReplacerClass);
                            PagingDevice paging = createWithArgs(pagingDeviceClass, new Class[] { PageReplacer.class }, new Object[] {pageReplacer});

                            IOScheduler ioScheduler = create(ioSchedulerClass);
                            IODevice io = createWithArgs(ioDeviceClass, new Class[]{IOScheduler.class}, new Object[]{ioScheduler});

                            // Register the dependant classes. io depends on ioScheduler and
                            // PagingDevice depends on PageReplacer

                            Simulation sim = new Simulation(scheduler, paging, io, pageReplacer, ioScheduler);

                            // Load the data
                            for(String data : source.getProcesses()) {
                                List<Instruction> instructions = m_processDeserializer.deserialize(data);
                                Process proc = new Process(instructions, paging, io);
                                sim.addProcess(proc);
                            }

                            sim.runToEnd();
                            set.addSimulation(sim);
                        }
                    }
                }
			}
		}
		return set;
	}
	
	/**
	 * Returns how many simulations will be built based on the current
	 * settings.
	 * 
	 * @return The number of simulations that will be run
	 */
	public int getNumberOfSimulationToRun() {
		return m_selectedSchedulers.size() * m_selectedPagingDevices.size() * m_selectedIODevices.size() * m_selectedIOSchedulers.size() * m_selectedPageReplacers.size();
	}

	/**
	 * Core generic function to register
	 * a given class as a possible option
	 * for configuring the simulation. This is
	 * used to allow us to configure different
	 * simulations from the UI.
	 * 
	 * @param list The list of classes to register clazz to.
	 * @param clazz The class to register with the list
	 */
	private <T> void register(List<Class<? extends T>> list, 
			Class<? extends T> clazz) {
		list.add(clazz);
	}
	
	private <T> T create(Class<T> clazz) {
		try {
			Constructor<T> constructor = clazz.getConstructor();
			System.out.println("Creating clazz: " + clazz.toString());
			return constructor.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

    private <T> T createWithArgs(Class<T> clazz, Class[] argTypes, Object[] args) {
        try {
            return clazz.getConstructor(argTypes).newInstance(args);
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

	/**
	 * Registration function
	 * 
	 * @param scheduler The scheduler class to register
	 */
	public void registerScheduler(Class<? extends ProcessScheduler> scheduler) {
		register(m_schedulers, scheduler);
	}

	/**
	 * Registration function
	 * 
	 * @param store The store class to register
	 */
	public void registerPagingDevice(Class<? extends PagingDevice> store) {
		register(m_pagingDevices, store);
	}

    public void registerPageReplacer(Class<? extends PageReplacer> replacer) {
        register(m_pageReplacers, replacer);
    }
	
	/**
	 * Registration function
	 * 
	 * @param device the IO device class to register
	 */
	public void registerIODevice(Class<? extends IODevice> device) {
		register(m_ioDevices, device);
	}
	
	public void registerIOScheduler(Class<? extends IOScheduler> scheduler) {
		register(m_ioSchedulers, scheduler);
	}

	// Getters
	public List<Class<? extends ProcessScheduler>> getSchedulers() {
		return m_schedulers;
	}
	
	public List<Class<? extends PagingDevice>> getPagingDevices() {
		return m_pagingDevices;
	}

    public List<Class<? extends PageReplacer>> getPageReplacers() {
        return m_pageReplacers;
    }
	
	public List<Class<? extends IODevice>> getIODevices() {
		return m_ioDevices;
	}
	
	public List<Class<? extends IOScheduler>> getIOSchedulers() {
		return m_ioSchedulers;
	}

	// Selectors
	public void selectScheduler(Class<? extends ProcessScheduler> scheduler, boolean selected) {
		selectDevice(m_selectedSchedulers, scheduler, selected);
	}
	
	public void selectPagingDevice(Class<? extends PagingDevice> paging, boolean selected) {
		selectDevice(m_selectedPagingDevices, paging, selected);
	}

    public void selectPageReplacer(Class<? extends PageReplacer> pageReplacer, boolean selected) {
        selectDevice(m_selectedPageReplacers, pageReplacer, selected);
    }
	
	public void selectIODevice(Class<? extends IODevice> io, boolean selected) {
		selectDevice(m_selectedIODevices, io, selected);
	}
	
	public void selectIOScheduler(Class<? extends IOScheduler> ioScheduler, boolean selected) {
		selectDevice(m_selectedIOSchedulers, ioScheduler, selected);
	}
	
	private <T> void selectDevice(List<Class<? extends T>> target, Class<? extends T> input, boolean selected) {
		System.out.println("Selected: " +selected);
		if(selected) { // Add to the target
			if(target.contains(input)) { 
				// If we already contain the input then we just want to ignore the request.
				System.out.println("Target already exists ignoring");
				return;
			}
			
			target.add(input);
			System.out.println("Adding target: " + target.toString());
		} else { // Remove from the target if it exists
			if(!target.contains(input)) {
				System.out.println("Ignoring failed remove");
				// If we don't contain the input and we're trying to remove then we don't need to do anything!
				return;
			}
			
			target.remove(input);
			System.out.println("Removing, new size: " + target.size() + " : " +target.toString());
		}
	}
	
	/**
	 * Set the ProcessDeserializer to use 
	 * 
	 * @param deserializer The process deserializer class to register
	 */
	public void setProcessDeserializer(ProcessDeserializer deserializer) {
		m_processDeserializer = deserializer;
	}
}
