package au.edu.newcastle.ossim.io.device;

import au.edu.newcastle.ossim.core.TimeSensitive;
import au.edu.newcastle.ossim.core.Utility;
import au.edu.newcastle.ossim.ui.component.timeline.TimelineModel;

import java.util.HashMap;
import java.util.Map;

/**
 * User: Jake Woods
 * Date: 3/07/13
 */
public class IORequestQueueHistorian implements TimelineModel, TimeSensitive {
    private int time = 0;
    private Map<Integer, String> history = new HashMap<Integer, String>();

    private IODevice ioDevice;

    public IORequestQueueHistorian(IODevice ioDevice) {
        this.ioDevice = ioDevice;
    }

    @Override
    public void update() {
        time += 1;
    }

    @Override
    public void step() {
        String requests = ioDevice.getRequests();
        if(requests != null) {
            history.put(time, requests);
        }
    }

    @Override
    public void reset() {
        time = 0;
        history.clear();
    }

    @Override
    public Map<Integer, String> getHistory() {
        return history;
    }

    @Override
    public Integer getLatestHistory() {
        return Utility.getLargestKeyInMap(history, 0);
    }

    public String toString() {
        return "Request Queue";
    }
}
