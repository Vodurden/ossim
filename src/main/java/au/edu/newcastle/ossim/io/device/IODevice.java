package au.edu.newcastle.ossim.io.device;

import au.edu.newcastle.ossim.core.TimeSensitive;
import au.edu.newcastle.ossim.cpu.execution.Instruction;

public interface IODevice extends TimeSensitive {
    /**
     * Adds a request from a given instruction to retrieve data at
     * the target.
     *
     * @param i An instruction owned by the process requesting the IO.
     * @param target The IO target to retrieve. Typically refers to a disk sector but doesn't have to.
     */
	public void addRequest(Instruction i, int target);

    /**
     * Attempts to complete the request from a given process. If this method returns true the request is considered
     * completed. Calling completeRequest from the same instruction twice will return false the second time as a
     * single request can only be completed once. If this method returns false the request is still processing
     * and has not completed yet.
     *
     * @param i An instruction owned by the process checking for IO completion.
     * @return The state of completion for a given request. True if complete. False otherwise.
     */
	public boolean completeRequest(Instruction i);

    /**
     * Returns a string representing the current state of requests. The actual format of this string
     * is undefined and will depend on implementation. If there are no requests this method returns null.
     *
     * @return A string representing the current state of requests. Or null if there are no requests.
     */
    public String getRequests();

    /**
     * Returns a string indicating the current request state. Depending on device this will differ
     * in format. If a request is currently processing this method returns an empty string.
     * If there is no active request this method returns null.
     *
     * @return The current request. Or null if there is no active request.
     */
    public String getCurrentRequest();
}
