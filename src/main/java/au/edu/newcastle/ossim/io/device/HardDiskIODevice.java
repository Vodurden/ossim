package au.edu.newcastle.ossim.io.device;

import au.edu.newcastle.ossim.annotation.DisplayName;
import au.edu.newcastle.ossim.io.scheduler.IOScheduler;

@DisplayName("Hard Disk")
public class HardDiskIODevice extends AbstractSchedulableIODevice {
	
	public HardDiskIODevice(IOScheduler scheduler) {
        super(scheduler);
	}
	
	private int currentTrack = 0;
	private int numTracks = 200;
	private int moveAmountPerStep = 1;

    private String state = null;

	@Override
	public void stepImpl() {
		int movementDone = 0;
		while(movementDone < moveAmountPerStep) {
			Request activeRequest = getActiveRequest();
			if(activeRequest == null) {
                state = null;
				return;
			}

            state = "Pos: " + currentTrack + ". Seek: " + activeRequest.target;

			currentTrack = moveCloser(currentTrack, activeRequest.target);
			movementDone += 1;
			
			if(currentTrack == activeRequest.target) {
				completeActiveRequest();

                state = "Load: " + activeRequest.target;
			}
		}
	}

    /**
     * Overload for moveCloser with step=1
     *
     * @param current The position to move from
	 * @param target The position to move to
     * @return The value current will be at when moved closer by 1.
     */
	private int moveCloser(int current, int target) {
		return moveCloser(current, target, 1);
	}
	
	/**
	 * Moves the current number closer to the current target by step.
	 * If step can reach the target the returned value is the target.
     * Because the disk is circular moving closer to a given target
     * requires the ability to move bidirectionally. Additionally it
     * means if we start at 95 with a target of 5 and our disk size
     * is 100 then the optimal path is to move higher from 95 -> 99 -> 0 -> 5.
	 * 
	 * @param current The position to move from
	 * @param target The position to move to
     * @param step The amount to move by
	 * @return The value current will be at when moved closer by step.
	 */
	private int moveCloser(int current, int target, int step) {
		if(current < target) {
			current += step;
			if(current > target) {
				return clampToTrack(target);
			}
			return current;
		} else if(current > target) {
			current -= step;
			if(current < target) {
				return clampToTrack(target);
			}
		}
		
		return clampToTrack(current);
	}
	
	/**
	 * Wraps integers that are outside the track so that they are
	 * in the track. I.e if the number of tracks is 200 then the number
	 * 205 will become 5. 200 -> 0. -50 -> 149 
	 * 
	 * @param input
	 * @return
	 */
	private int clampToTrack(int input) {
		if(input >= numTracks) {
			return (input % numTracks);
		} else {
			return numTracks + (input % numTracks);
		}
	}

    @Override
    public String getCurrentRequest() {
        return state;
    }
}
