package au.edu.newcastle.ossim.io.device;

import au.edu.newcastle.ossim.cpu.execution.Instruction;
import au.edu.newcastle.ossim.io.scheduler.IOScheduler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractSchedulableIODevice implements IODevice {
	
	protected class Request {
		public Integer target;
		public Instruction requester;
		
		public Request(Integer target, Instruction requester) {
			this.target = target;
			this.requester = requester;
		}
	}
	
	private List<Request> requests = new ArrayList<Request>();
	private List<Request> completedRequests = new ArrayList<Request>();
	private IOScheduler scheduler;

    private Request activeRequest = null;

    public AbstractSchedulableIODevice(IOScheduler scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public String getRequests() {
        if(requests.isEmpty()) {
            return null;
        }

        StringBuilder stringBuilder = new StringBuilder();

        // We want to turn requests into a comma separate string. Unfortunately native java doesn't have anything
        // useful (like LINQ) so we're going to do it ourselves.
        String sep = "";
        for(Request request : requests) {
            stringBuilder.append(sep);
            stringBuilder.append(request.target);
            sep = ",";
        }

        return stringBuilder.toString();
    }

	@Override
	public final void addRequest(Instruction i, int target) {
		requests.add(new Request(target, i));
	}

	@Override
	public final boolean completeRequest(Instruction i) {
		// Find the request with the corresponding instruction.
		Iterator<Request> iterator = completedRequests.iterator();
		boolean found = false;
		while(iterator.hasNext()) {
			Request request = iterator.next();
			// Here we use == because we're trying
			// to find the exact same object, not something
			// that is "logically" equivalent.
			if(request.requester == i) {
				found = true;
				iterator.remove();
				break;
			}
		}
		
		return found;
	}
	
	@Override
	public final void step() {
        stepImpl();
    }

    public final void reset() {
        requests.clear();
        completedRequests.clear();
    }

    public final void update() {
    }

    protected abstract void stepImpl();
	
	/**
	 * Returns the active request.
	 * 
	 * @return The active request or null if there are no requests
	 */
	protected Request getActiveRequest() {
		if(activeRequest == null) {
			if(requests.isEmpty()) {
				return null;
			}
			
			activeRequest = popNextRequest();
		}
		
		return activeRequest;
	}
	
	/**
	 * Completes the currently active request. If there are no active
	 * requests this method does nothing.
	 */
	protected void completeActiveRequest() {
		completedRequests.add(activeRequest);
		activeRequest = null;
	}
	
	private Request popNextRequest() {
		// Create an integer list from the request list.
		List<Integer> ioTargets = new ArrayList<Integer>();
		for(Request request : requests) {
			ioTargets.add(request.target);
		}
		
		Integer selectedIndex = scheduler.selectNextTargetIndex(ioTargets);
		
		// We need to downcast from Integer -> int because the method
		// list.remove(Object) returns true/false whereas the method
		// list.remove(int) returns the object at the index that was removed.
		activeRequest = requests.remove((int)selectedIndex);
		return activeRequest;
	}
	
}