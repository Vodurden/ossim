package au.edu.newcastle.ossim.io.scheduler;

import au.edu.newcastle.ossim.annotation.DisplayName;

import java.util.List;

@DisplayName("First Come First Serve")
public class FirstComeFirstServeIOScheduler implements IOScheduler {

	/**
	 * Selects the index of the next IO target. For example if the request list 
	 * is [5, 10, 15] and we select 5, this method will return 0. (The index of 5)
	 * 
	 * @param requests The requests to select from
	 * @return The index of the selected request.
	 */
	@Override
	public Integer selectNextTargetIndex(List<Integer> requests) {
		return 0;
	}
}