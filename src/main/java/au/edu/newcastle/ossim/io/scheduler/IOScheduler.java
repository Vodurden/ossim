package au.edu.newcastle.ossim.io.scheduler;

import java.util.List;

public interface IOScheduler {
	public Integer selectNextTargetIndex(List<Integer> requests); 
}
