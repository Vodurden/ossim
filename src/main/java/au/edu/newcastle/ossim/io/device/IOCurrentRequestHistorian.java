package au.edu.newcastle.ossim.io.device;

import au.edu.newcastle.ossim.core.TimeSensitive;
import au.edu.newcastle.ossim.core.Utility;
import au.edu.newcastle.ossim.ui.component.timeline.TimelineModel;

import java.util.HashMap;
import java.util.Map;

/**
 * User: Jake Woods
 * Date: 3/07/13
 *
 * This class is responsible for recording the history
 * of an IO device so it can be used in a timeline.
 */
public class IOCurrentRequestHistorian implements TimelineModel, TimeSensitive {

    private int time = 0;
    private Map<Integer, String> history = new HashMap<Integer, String>();

    private IODevice ioDevice;

    public IOCurrentRequestHistorian(IODevice ioDevice) {
        this.ioDevice = ioDevice;
    }

    public void update() {
        time += 1;
    }

    public void step() {
        String request = ioDevice.getCurrentRequest();
        if(request != null) {
            history.put(time, ioDevice.getCurrentRequest());
        }
    }

    public void reset() {
        time = 0;
        history.clear();
    }

    @Override
    public Map<Integer, String> getHistory() {
        return history;
    }

    @Override
    public Integer getLatestHistory() {
        return Utility.getLargestKeyInMap(history, 0);
    }

    @Override
    public String toString() {
        return "Current IO Request";
    }
}
