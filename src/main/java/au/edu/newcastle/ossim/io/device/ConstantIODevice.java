package au.edu.newcastle.ossim.io.device;

import au.edu.newcastle.ossim.annotation.DisplayName;
import au.edu.newcastle.ossim.io.scheduler.IOScheduler;

/**
 * Simulates an IO device that always
 * accesses IO in a constant number of
 * steps. By default the amount of
 * steps is 5
 * 
 * @author Jake Woods
 */
@DisplayName("Constant Retrival")
public class ConstantIODevice extends AbstractSchedulableIODevice {
	// How many steps it takes to complete one IO request
	private int steps = 5;
	
	// The current step of the current request
	private int currentStep;

    private String state = null;
    private boolean processing = false;
	
	public ConstantIODevice(IOScheduler scheduler) {
        super(scheduler);

		currentStep = steps;
	}
	
	@Override
	public void stepImpl() {
		Request activeRequest = getActiveRequest();
		if(activeRequest == null) {
            state = null;
			return;
		}

        if(!processing) {
            state = "Seek: " + activeRequest.target;
            processing = true;
        } else {
            state = "";
        }
		
		// Check if we've finished the active request.
		currentStep -= 1;
		
		if(currentStep == 0) {
			completeActiveRequest();
			currentStep = steps;

            processing = false;
            state = "Load: " + activeRequest.target;
		}
	}

    @Override
    public String getCurrentRequest() {
        return state;
    }
}
