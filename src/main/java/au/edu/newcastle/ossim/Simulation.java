package au.edu.newcastle.ossim;

import au.edu.newcastle.ossim.cpu.execution.Process;
import au.edu.newcastle.ossim.cpu.scheduler.ProcessScheduler;
import au.edu.newcastle.ossim.io.device.IOCurrentRequestHistorian;
import au.edu.newcastle.ossim.io.device.IODevice;
import au.edu.newcastle.ossim.io.device.IORequestQueueHistorian;
import au.edu.newcastle.ossim.io.scheduler.IOScheduler;
import au.edu.newcastle.ossim.paging.device.PagingDevice;
import au.edu.newcastle.ossim.paging.replacer.PageReplacer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Simulation {

    /**
     * Stores a reference to each process as they are entered, this lets us know
     * the input order so we can correctly reset the simulation on request.
     */
    private List<Process> m_processOrder = new ArrayList<Process>();

	/**
	 * Contains all the processes that can be
	 * selected from by the ProcessScheduler
	 */
	private List<Process> m_ready = new ArrayList<Process>();
	
	/**
	 * Contains all the processes that are currently
	 * blocking. Common causes of blocking are
	 * waiting on IO or for a resource request
	 * to finish
	 */
	private List<Process> m_blocked = new ArrayList<Process>();
	
	/**
	 * Contains all the processes that are going to
	 * be blocked at the end of this step. This is required
	 * due to the way step processing is done in order to 
	 * prevent double-stepping of blocked processes.
	 */
	private List<Process> m_pendingBlocked = new ArrayList<Process>();
	
	/**
	 * Contains all the processes that have finished (executed all
	 * of their instructions)
	 */
	private List<Process> m_finished = new ArrayList<Process>();
	

	public List<Process> getReadyList() {
		return m_ready;
	}
	
	public List<Process> getBlockedList() {
		return m_blocked;
	}
	
	public List<Process> getFinishedList() {
		return m_finished;
	}
	
	public List<Process> getAllProcesses() {
		List<Process> all = new ArrayList<Process>();
		all.addAll(getReadyList());
		all.addAll(getBlockedList());
		all.addAll(getFinishedList());
		return all;
	}
	
	private ProcessScheduler scheduler;
	private PagingDevice paging;

	private IODevice ioDevice;
    private IOCurrentRequestHistorian ioCurrentRequestHistorian;
    private IORequestQueueHistorian ioRequestQueueHistorian;

    // The page replacer and IO Scheduler are recorded here just so we can
    // display them in the simulation set, they aren't actually used in the class
    private PageReplacer m_pageReplacer;
    private IOScheduler m_ioScheduler;

	public Simulation(ProcessScheduler scheduler, PagingDevice paging, IODevice io, PageReplacer pageReplacer, IOScheduler ioScheduler) {
		this.scheduler = scheduler;
		this.paging = paging;
		this.ioDevice = io;

        this.ioCurrentRequestHistorian = new IOCurrentRequestHistorian(this.ioDevice);
        this.ioRequestQueueHistorian = new IORequestQueueHistorian(this.ioDevice);

        this.m_pageReplacer = pageReplacer;
        this.m_ioScheduler = ioScheduler;
	}
	
	public ProcessScheduler getScheduler() {
		return scheduler;
	}

	public PagingDevice getPagingDevice() {
		return paging;
	}

    public PageReplacer getPageReplacer() {
        return m_pageReplacer;
    }

	public IODevice getIODevice() {
		return ioDevice;
	}

    public IOScheduler getIOScheduler() {
        return m_ioScheduler;
    }

    public IOCurrentRequestHistorian getIOCurrentRequestHistorian() {
        return ioCurrentRequestHistorian;
    }

    public IORequestQueueHistorian getIORequestQueueHistorian() {
        return ioRequestQueueHistorian;
    }

	public int getTotalTimeToComplete() {
		int total = 0;
		for(Process p : getAllProcesses()) {
			total += p.getTimeToComplete();
		}
		return total;
	}
	
	public int getTotalTimeSpentWaiting() {
		int total = 0;
		for(Process p : getAllProcesses()) {
			total += p.getTimeSpentBlocked();
		}
		return total;
	}

    private void updateHistory() {
        ioCurrentRequestHistorian.update();
        ioRequestQueueHistorian.update();
    }
	
	private void updateReady() {
		// Go through the ready queue until
		// we we hit a process that executes
		// a non-blocking instruction
		while(!m_ready.isEmpty()) {
			Process currentProcess = scheduler.selectNextProcess(m_ready);
			
			// Remove the selected process from m_ready
			m_ready.remove(currentProcess);
			
			currentProcess.step();
			if(!currentProcess.blocked()) {
				// If we aren't blocked add the executed process
				// to the back of the ready queue and continue
				m_ready.add(currentProcess);
				break;
			} else {
				m_pendingBlocked.add(currentProcess);
			}
		}
	}
	
	private void updateBlocked() {
		// Go through the blocked queue and see if
		// anything has finished
		Iterator<Process> blockedIt= m_blocked.iterator();
		while(blockedIt.hasNext()) {
			Process p = blockedIt.next();
			p.step();
			
			if(!p.blocked()) {
				m_ready.add(p);
				blockedIt.remove();
			}
		}
	}
	
	private void updatePendingBlocked() {
		m_blocked.addAll(m_pendingBlocked);
		m_pendingBlocked.clear();
	}
	
	private void checkForFinished(List<Process> list) {
		// Go through all the processes and 
		// move any finished ones to m_finished
		Iterator<Process> finishedIt = list.iterator();
		while(finishedIt.hasNext()) {
			Process p = finishedIt.next();
			p.update();
			if(p.finished()) {
				m_finished.add(p);
				finishedIt.remove();
			}
		}
	}
	
	public void addProcess(Process p) {
		m_ready.add(p);
        m_processOrder.add(p);
	}
	
	public void step() {
        ioDevice.step();
        paging.step();
        ioCurrentRequestHistorian.step();
        ioRequestQueueHistorian.step();

        updateReady();
        updateBlocked();
        updatePendingBlocked();
        checkForFinished(m_ready);
        checkForFinished(m_blocked);
        updateHistory();
	}
	
	public void runToEnd() {
		while(!finished()) {
			step();
		}
	}

    public void reset() {
        // We have a list of processes sorted by input order from the m_processOrder list.
        // all we need to do is remove the references from the ready, blocked and finish lists and then
        // we simply reset() each process and add the contents of m_processOrder to m_ready which effectively
        // resets the simulation.
        m_ready.clear();
        m_blocked.clear();
        m_finished.clear();

        for(Process p : m_processOrder) {
            p.reset();
            m_ready.add(p);
        }

        // Reset the devices
        this.ioDevice.reset();
        this.paging.reset();
        this.ioCurrentRequestHistorian.reset();
        this.ioRequestQueueHistorian.reset();
    }
	
	public Boolean finished() {
		return (m_ready.isEmpty() && m_blocked.isEmpty());
	}
}
