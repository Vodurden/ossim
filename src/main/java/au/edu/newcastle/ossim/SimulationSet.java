package au.edu.newcastle.ossim;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a set of simulations that can be run and compared
 * against.
 *
 * @author Jake Woods
 */
public class SimulationSet {
	private List<Simulation> m_simulations = new ArrayList<Simulation>();
	
	public SimulationSet() {
		
	}
	
	public void addSimulation(Simulation simulation) {
		m_simulations.add(simulation);
	}
	
	public List<Simulation> getSimulations() {
		return m_simulations;
	}
	
	public Simulation getSimulation(int id) {
		return m_simulations.get(id);
	}
}
