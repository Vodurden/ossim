package au.edu.newcastle.ossim.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


public @Retention(RetentionPolicy.RUNTIME) @interface DisplayName {
	String value();
}