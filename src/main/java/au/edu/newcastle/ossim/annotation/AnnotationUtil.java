package au.edu.newcastle.ossim.annotation;

import java.lang.annotation.Annotation;

public class AnnotationUtil {

	/**
	 * Retrieves an annotation of class annotationClass from the concrete type of
	 * baseClass and returns it. If no such annotation is found this method
	 * returns null.
	 * 
	 * @param baseClass The base class to downcast
	 * @param annotationClass The annotation class to retrieve
	 * @return The request annotation on success, or null
	 */
	public static <T extends Annotation> T getAnnotationFromSubclass(Class<?> baseClass, Class<T> annotationClass) {
		Class<?> subclass = null;
		try {
			subclass = Class.forName(baseClass.getName());
		} catch (ClassNotFoundException e) {
			System.err.println("Failed to downcast class");
		}
		
		// Default highest level name, just the class name
		if(subclass != null) {
			T annotation = subclass.getAnnotation(annotationClass);
			if(annotation != null) {
				return annotation;
			}
		}
		
		return null;
			
	}

	public static String getDisplayName(Class<?> baseClass) {
		DisplayName annotation = getAnnotationFromSubclass(baseClass, DisplayName.class);
		String name = baseClass.getSimpleName();
		if(annotation != null) {
			name = annotation.value();
		}
		
		return name;
	}
}
