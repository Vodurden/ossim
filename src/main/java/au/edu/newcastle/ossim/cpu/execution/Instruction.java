package au.edu.newcastle.ossim.cpu.execution;

import au.edu.newcastle.ossim.core.TimeSensitive;
import au.edu.newcastle.ossim.io.device.IODevice;
import au.edu.newcastle.ossim.paging.device.PagingDevice;

/**
 * Common interface for all instructions
 * 
 * @author Jake Woodss
 */
public abstract class Instruction implements TimeSensitive {
	
	/**
	 * Resets an instruction to it's original state
	 */
	public abstract void reset();
	
	/**
	 * Simulates one unit of time on this instruction
	 * <p>
	 * It is important to note that not all instructions
	 * will complete in 1 unit of time, for example
	 * IO Operations can take many units of time
	 * to execute.
	 */
	public abstract void step();

    public void update() {}
	
	/**
	 * @return true if the instruction has finished executing
	 */
	public abstract Boolean done();
	
	/**
	 * @return true if we should block the process while executing this instruction
	 */
	public abstract Boolean blocking();
	
	/**
	 * The following section contains different event registrations, a class can choose
	 * to implement none, some or all of these and by doing so suggests they are
	 * interested in the particular device. For example IOInstruction implements
	 * register(IODevice) which suggests it is interested in something to do with the
	 * IO device.
	 */
	public void register(Process parent) {}
	public void register(IODevice io) {}
	public void register(PagingDevice paging) {}
}
