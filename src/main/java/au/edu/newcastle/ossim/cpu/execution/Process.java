package au.edu.newcastle.ossim.cpu.execution;

import au.edu.newcastle.ossim.core.TimeSensitive;
import au.edu.newcastle.ossim.core.Utility;
import au.edu.newcastle.ossim.io.device.IODevice;
import au.edu.newcastle.ossim.paging.device.PagingDevice;
import au.edu.newcastle.ossim.ui.component.timeline.TimelineModel;

import java.util.*;


public class Process implements TimelineModel, TimeSensitive {
	private static Integer s_lastId = 1;
	
	private Integer m_id;
	private List<Instruction> m_instructions = new ArrayList<Instruction>();
	
	private Instruction m_curInstruction;
	private Iterator<Instruction> m_curInstructionIterator; 
	private Integer m_currentInstructionIndex; // Only used to determine SRT
	
	// Stores a history of the instructions executing at each step
	private Integer m_time = 0;
	private Map<Integer, String> history = new HashMap<Integer, String>();
	 
	private Boolean m_blocked = false;
	private Boolean m_finished = false;
	
	// Devices
	private PagingDevice m_paging;
	private IODevice m_io;

	// Statistics
	private int m_timeSpentBlocked = 0;
	private int m_timeToComplete = 0;
	
	public Process(List<Instruction> instructions, PagingDevice paging, IODevice io) {
		this.m_id = s_lastId++;
		this.m_instructions = instructions;
		
		this.m_paging = paging;
		this.m_io = io;
		
		reset();
	}
	
	public Integer getID() {
		return m_id;
	}
	
	public void reset() {
		for(Instruction i : m_instructions) {
			i.reset();
		}
		
		// TODO: Consider checking for hasNext here
		m_currentInstructionIndex = 0;
		m_curInstructionIterator = m_instructions.iterator();
		m_curInstruction = m_curInstructionIterator.next();
		m_blocked = m_curInstruction.blocking();
		m_finished = false;
		
		// Register all instructions with our devices and register ourselves as the parent.
		for(Instruction i : m_instructions) {
			i.register(m_io);
			i.register(m_paging); 
			i.register(this); 
		}
		
		// Reset time and history
		m_time = 0;
		m_timeToComplete = 0;
		m_timeSpentBlocked = 0;
		history.clear();
	}
	
	public void step() {
		System.out.println("Process " + m_id + " doing instruction " + m_curInstruction.toString());
		
		m_curInstruction.step();
		m_blocked = m_curInstruction.blocking();
		
		// Update our history
		history.put(m_time, m_curInstruction.toString());
	}
	
	public void update() {
		// Update the time 
		m_time += 1;
		m_timeToComplete += 1;
		if(m_blocked) {
			m_timeSpentBlocked += 1;
		}
		
		if(m_curInstruction.done()) {
			if(m_curInstructionIterator.hasNext()) {
				m_curInstruction = m_curInstructionIterator.next();
				m_currentInstructionIndex += 1;
			} else {
				m_finished = true;
				return;
			}
		}
		
		m_blocked = m_curInstruction.blocking();
	}
	
	public Boolean blocked() {
		return m_blocked;
	}
	
	public Boolean finished() {
		return m_finished;
	}
	
	public int getTimeToComplete() {
		return m_timeToComplete;
	}
	
	public int getTimeSpentBlocked() {
		return m_timeSpentBlocked;
	}
	
	public Integer remainingTime() {
		return m_instructions.size() - m_currentInstructionIndex;
	}
	
	@Override
	public Integer getLatestHistory() {
        return Utility.getLargestKeyInMap(history, 0);
	}
	
	public void addInstruction(Instruction instruction) {
		m_instructions.add(instruction);
	}
	
	
	public List<Instruction> getInstructions() {
		return m_instructions;
	}
	
	public Instruction getCurrentInstruction() {
		return m_curInstruction;
	}
	
	@Override
	public Map<Integer, String> getHistory() {
		return history;
	}
	
	@Override
	public String toString() {
		return "Process " + m_id;
	}
}
