package au.edu.newcastle.ossim.cpu.execution;

import au.edu.newcastle.ossim.io.device.IODevice;

public class IOInstruction extends Instruction {
	
	private int m_fileLocation; // Location of file sector on the disk
	private boolean m_hasRequested;
	private boolean m_done;
	private String m_state;
	
	private IODevice m_io;
	
	public IOInstruction(int fileLocation) {
		m_fileLocation = fileLocation;
		reset();
	}

	@Override
	public void reset() {
		m_done = false;
		m_hasRequested = false;
		m_io = null;
		m_state = "";
	}

	@Override
	public void step() {
		// On the first step, request IO from the IO device
		// otherwise wait for an event and finish when we recieve it
		m_state = "";
		
		if(!m_hasRequested) {
			m_io.addRequest(this, m_fileLocation);
			m_hasRequested = true;
			m_state = "RQ I" + m_fileLocation;
		} else {
			if(m_io.completeRequest(this)) {
				m_done = true;
				m_state = "EX I" + m_fileLocation;
			}
		}
	}

	@Override
	public Boolean done() {
		return m_done;
	}

	@Override
	public Boolean blocking() {
		// We block if we're not done
		return !m_done;
	}
	
	// We are interested in IO Devices
	@Override
	public void register(IODevice io) {
		m_io = io;
	}
	
	@Override
	public String toString() {
		return m_state;
	}
}