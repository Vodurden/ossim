package au.edu.newcastle.ossim.cpu.scheduler;

import au.edu.newcastle.ossim.annotation.DisplayName;
import au.edu.newcastle.ossim.cpu.execution.Process;

import java.util.List;

@DisplayName("Round Robin")
public class RoundRobinProcessScheduler implements ProcessScheduler {

	@Override
	public Process selectNextProcess(List<Process> choices) {
		return choices.get(0);
	}
	
}
