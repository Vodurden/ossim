package au.edu.newcastle.ossim.cpu.execution;

import au.edu.newcastle.ossim.paging.device.PagingDevice;

public class CPUInstruction extends Instruction {
	
	private int m_page;
	private boolean m_hasRequested;
	private boolean m_done;
	private String m_state;
	
	private Process m_parent;
	private PagingDevice m_paging;
	
	public CPUInstruction(int page) {
		m_page = page;
		reset();
	}
	
	@Override
	public void reset() {
		m_done = false;
		m_hasRequested = false;
		m_paging = null;
		m_parent = null;
		m_state = "";
	}

	@Override
	public void step() {
		m_state = "";
		
		if(m_paging.isLoaded(m_parent, m_page)) {
			m_done = true;
			
			m_state = "EX C" + m_page;
			m_paging.notifyOfUse(m_parent, m_page);
		} else {
			if(!m_hasRequested) {
				m_paging.load(m_parent, m_page);
				
				m_state = "LD C" + m_page;
			}
		}
	}

	@Override
	public Boolean done() {
		return m_done;
	}

	@Override
	public Boolean blocking() {
		return !m_done; // We block if we are loading an instruction
	}
	
	@Override
	public void register(PagingDevice paging) {
		m_paging = paging;
	}
	
	@Override
	public void register(Process parent) {
		m_parent = parent;
	}
	
	@Override
	public String toString() {
		return m_state;
	}
}
