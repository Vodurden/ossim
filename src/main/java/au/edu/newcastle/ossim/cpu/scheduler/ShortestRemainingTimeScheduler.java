package au.edu.newcastle.ossim.cpu.scheduler;

import au.edu.newcastle.ossim.annotation.DisplayName;
import au.edu.newcastle.ossim.cpu.execution.Process;

import java.util.List;

@DisplayName("Shortest Remaining Time")
public class ShortestRemainingTimeScheduler implements ProcessScheduler {

	@Override
	public Process selectNextProcess(List<Process> choices) {
		Process next = null;
		for(Process p : choices) {
			if(next == null || next.remainingTime() > p.remainingTime()) {
				next = p;
			}
		}
		return next;
	}

}
