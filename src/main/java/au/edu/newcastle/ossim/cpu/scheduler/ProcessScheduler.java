package au.edu.newcastle.ossim.cpu.scheduler;

import au.edu.newcastle.ossim.cpu.execution.Process;

import java.util.List;

public interface ProcessScheduler {
	
	/**
	 * Selects the next process to compute
	 * from the list of choices
	 * 
	 * @param choices The list of possible processes to select from
	 * @return The process to simulate next
	 */
	public abstract Process selectNextProcess(List<Process> choices);
}
