package au.edu.newcastle.ossim;

import au.edu.newcastle.ossim.cpu.scheduler.RoundRobinProcessScheduler;
import au.edu.newcastle.ossim.cpu.scheduler.ShortestRemainingTimeScheduler;
import au.edu.newcastle.ossim.io.device.ConstantIODevice;
import au.edu.newcastle.ossim.io.device.HardDiskIODevice;
import au.edu.newcastle.ossim.io.scheduler.FirstComeFirstServeIOScheduler;
import au.edu.newcastle.ossim.loader.Assignment1InstructionDeserializer;
import au.edu.newcastle.ossim.loader.NewlineProcessDeserializer;
import au.edu.newcastle.ossim.loader.ProcessDeserializer;
import au.edu.newcastle.ossim.paging.device.InfinitePagingDevice;
import au.edu.newcastle.ossim.paging.replacer.FirstInFirstOutPageReplacer;
import au.edu.newcastle.ossim.ui.SimulationUI;


public class Simulator {
	/**
	 * Adds a process into the simulator.
	 * 
	 * @param process The process to add
	 */
	
	// Test Main function, students would implement something like this
	// in their own class using Simulator
	public static void main(String[] args) {
		// First we need to select the type of processes we are using
		ProcessDeserializer deserializer = new NewlineProcessDeserializer(new Assignment1InstructionDeserializer());
		SimulationManager superman = new SimulationManager(deserializer);
		
		// Process Schedulers -- Controls what process gets selected first.
		superman.registerScheduler(RoundRobinProcessScheduler.class);
		superman.registerScheduler(ShortestRemainingTimeScheduler.class);
		
		// Paging Devices -- Controls how CPU instructions are loaded and executed
		superman.registerPagingDevice(InfinitePagingDevice.class);

        // Page Replacers -- Controls what pages are selected for replacement
        superman.registerPageReplacer(FirstInFirstOutPageReplacer.class);
		
		// IO Devices -- Controls how quickly an IO request can be fulfilled 
		superman.registerIODevice(ConstantIODevice.class);
		superman.registerIODevice(HardDiskIODevice.class);
		
		// IO Schedulers -- Controls what IO request gets fulfilled first.
		superman.registerIOScheduler(FirstComeFirstServeIOScheduler.class);
		
		SimulationUI ui = new SimulationUI(superman);
		ui.show();
	}
}
