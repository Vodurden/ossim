package au.edu.newcastle.ossim.loader;

import au.edu.newcastle.ossim.annotation.DisplayName;
import au.edu.newcastle.ossim.cpu.execution.Instruction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * A ProcessDeserializer is responsible for splitting files into a set of instructions that 
 * can be consumed by a InstructionDeserializer. In the case of the NewlineProcessDeserializer, 
 * an instruction will be created for each line in the input string.
 * 
 * @see ProcessDeserializer
 * @see InstructionDeserializer
 * @author Jake Woodss
 */
@DisplayName("Line-Based Instructions")
public class NewlineProcessDeserializer implements ProcessDeserializer {
	
	private InstructionDeserializer m_instructionDeserializer = null;
	
	public NewlineProcessDeserializer(InstructionDeserializer instructionDeserializer) {
		m_instructionDeserializer = instructionDeserializer;
	}
	
	@Override
	public List<Instruction> deserialize(String data) {
		List<Instruction> instructions = new ArrayList<Instruction>();
		
		BufferedReader reader = new BufferedReader(new StringReader(data));
		String line;
		try {
			while((line = reader.readLine()) != null) {
				// Remove all the whitespace from the string
				line = line.replaceAll("\\s", ""); 
				Instruction instruction = m_instructionDeserializer.deserialize(line);
				instructions.add(instruction);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		return instructions;
	}

}
