package au.edu.newcastle.ossim.loader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of ProcessLoader that loads
 * all processes from a single directory.
 * 
 * @author Jake Woods 
 */
public class FileSystemDataSource implements ProcessDataSource {
	
	private List<String> m_processes = new ArrayList<String>();
	
	/**
	 * @param directory The directory path to load the processes from
	 * @throws IOException
	 */
	public FileSystemDataSource(String directory) throws IOException {
		File rootDirectory = new File(directory);
		
		// Ensure that the passed directory is actually a directory
		if(!rootDirectory.isDirectory()) {
			// In java 7 we can use NotDirectoryException
			// but earlier we can't
			throw new IOException();
		}
		
		// Iterate through each file and add the resulting process
		// to the process list
		for(File child : rootDirectory.listFiles()) {
			// Ignore additional directories (do not recurse)
			if(child.isDirectory()) {
				continue;
			}
			
			// Convert the file into a string
			m_processes.add(convertToString(child));
		}
	}
	
	@Override
	public List<String> getProcesses() {
		return m_processes;
	}

	/**
	 * Converts a file to it's equivalent string representation.
	 * <p>
	 * If different character encodings are required this code will need to be
	 * modified to take that into account, see the included link for
	 * details on how to do this
	 * <p>
	 * This code is adapted from http://stackoverflow.com/a/326531/203133
	 * 
	 * @param f The file to convert
	 * @return The string representation of the file
	 * @throws IOException
	 */
	private String convertToString(File f) throws IOException {
		FileInputStream stream = new FileInputStream(f);
		try {
	        Reader reader = new BufferedReader(new InputStreamReader(stream));
	        StringBuilder builder = new StringBuilder();
	        char[] buffer = new char[8192];
	        int read;
	        while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
	            builder.append(buffer, 0, read);
	        }
	        return builder.toString();
		} finally {
			stream.close();
		}
	}

}
