package au.edu.newcastle.ossim.loader;

import au.edu.newcastle.ossim.cpu.execution.Instruction;

import java.util.List;

public interface ProcessDeserializer {
	public List<Instruction> deserialize(String data);
}
