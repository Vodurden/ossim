package au.edu.newcastle.ossim.loader;

import java.util.List;

public interface ProcessDataSource {
	/**
	 * Retrieves a list of processes
	 * as a string
	 * 
	 * @return
	 */
	public List<String> getProcesses();
}