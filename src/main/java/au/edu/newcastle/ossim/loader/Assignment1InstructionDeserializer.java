package au.edu.newcastle.ossim.loader;

import au.edu.newcastle.ossim.annotation.DisplayName;
import au.edu.newcastle.ossim.cpu.execution.CPUInstruction;
import au.edu.newcastle.ossim.cpu.execution.IOInstruction;
import au.edu.newcastle.ossim.cpu.execution.Instruction;

@DisplayName("C and I")
public class Assignment1InstructionDeserializer implements InstructionDeserializer {

	@Override
	public Instruction deserialize(String data) {
		if(data.contains(",")) {
			// Split the string into two parts delimited
			// by a comma. For example, if the string was
			// "1,c" then the parts contains ["1", "c"]
			String[] parts = data.split(",");
			
			// Get the second half of the split
			String instructionType = parts[0];
			Integer instructionData = Integer.parseInt(parts[1].trim());
			
			// Convert the string representation of the instruction to a proper
			// instruction class
			if(instructionType.equals("C")) {
				return new CPUInstruction(instructionData);
			} else if(instructionType.equals("I")) {
				return new IOInstruction(instructionData);
			}
		}
		 
		return null;
	}

}
