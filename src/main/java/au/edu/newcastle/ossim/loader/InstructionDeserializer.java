package au.edu.newcastle.ossim.loader;

import au.edu.newcastle.ossim.cpu.execution.Instruction;

public interface InstructionDeserializer {
	public Instruction deserialize(String data);
}
