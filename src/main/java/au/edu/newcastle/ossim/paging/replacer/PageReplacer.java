package au.edu.newcastle.ossim.paging.replacer;

public interface PageReplacer {
    public int selectNextSwapIndex(PageInfo[] pages);
}
