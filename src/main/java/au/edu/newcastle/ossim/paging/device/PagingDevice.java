package au.edu.newcastle.ossim.paging.device;

import au.edu.newcastle.ossim.core.TimeSensitive;
import au.edu.newcastle.ossim.cpu.execution.Process;

public interface PagingDevice extends TimeSensitive {
	/**
	 * @param p the process to check
	 * @return true if the given process is already loaded in memory
	 */
	public boolean isLoaded(Process p, int page);
	
	public void load(Process p, int page);
	
	public void notifyOfUse(Process p, int page);
}