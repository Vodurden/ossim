package au.edu.newcastle.ossim.paging.replacer;

import au.edu.newcastle.ossim.annotation.DisplayName;

@DisplayName("First In First Out")
public class FirstInFirstOutPageReplacer implements PageReplacer {
    @Override
    public int selectNextSwapIndex(PageInfo[] pages) {
        // We want to return the page that was added longest ago. In the case
        // of a tie we default to the one furthest along in memory.
        int currentLongestTimeSinceAdded = 0;
        int currentPageIndex = 0;
        for(int index = 0; index < pages.length; ++index) {
           PageInfo page = pages[index];
            if(page.getTimeSinceAdded() >= currentLongestTimeSinceAdded) {
                currentPageIndex = index;
                currentLongestTimeSinceAdded = page.getTimeSinceAdded();
            }
        }

        return currentPageIndex;
    }
}
