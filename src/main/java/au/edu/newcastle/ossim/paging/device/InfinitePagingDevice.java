package au.edu.newcastle.ossim.paging.device;

import au.edu.newcastle.ossim.annotation.DisplayName;
import au.edu.newcastle.ossim.cpu.execution.Process;
import au.edu.newcastle.ossim.paging.replacer.PageReplacer;

@DisplayName("Infinite Pagefile")
public class InfinitePagingDevice implements PagingDevice {

    public InfinitePagingDevice(PageReplacer replacer) {

    }

	@Override
	public boolean isLoaded(Process p, int page) {
		return true;
	}

	@Override
	public void load(Process p, int page) {
		// Do nothing, everything is considered loaded
	}
	
	@Override
	public void notifyOfUse(Process p, int page) {
		// We don't care what pages are used as we are infinite!
	}

    @Override
    public void update() {
        // Do nothing, we're infinite and immutable and unchanging!
    }

    @Override
	public void step() {
		// Do nothing, we don't simulate since everything is considered loaded
	}

    @Override
    public void reset() {
        // Do nothing, we're infinite and immutable and unchanging!
    }
}
