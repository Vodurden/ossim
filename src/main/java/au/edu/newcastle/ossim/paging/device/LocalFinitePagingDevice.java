package au.edu.newcastle.ossim.paging.device;

import au.edu.newcastle.ossim.annotation.DisplayName;
import au.edu.newcastle.ossim.cpu.execution.Process;
import au.edu.newcastle.ossim.paging.replacer.PageInfo;
import au.edu.newcastle.ossim.paging.replacer.PageReplacer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@DisplayName("Local Finite Pagefile")
public class LocalFinitePagingDevice implements PagingDevice{

    private final int pagesPerProcess = 2;
    private final int pageLoadSpeed = 5;

    private PageReplacer replacer;

    private class LoadingPage {
        public int pageNumber;
        public int loadingTime;
        public int targetMemoryIndex;
        public LoadingPage(int pageNumber, int loadingTime, int targetMemoryIndex) {
            this.pageNumber = pageNumber;
            this.loadingTime = loadingTime;
            this.targetMemoryIndex = targetMemoryIndex;
        }
}

    private Map<Process, PageInfo[]> pages = new HashMap<Process, PageInfo[]>();
    private Map<Process, LoadingPage> loadingPages = new HashMap<Process, LoadingPage>();

    public LocalFinitePagingDevice(PageReplacer replacer) {
        this.replacer = replacer;
    }

	@Override
	public boolean isLoaded(Process p, int pageNumber) {
        for(PageInfo page : pages.get(p)) {
            if(page.getPageNumber() == pageNumber) {
                return true;
            }
        }

		return false;
	}

	@Override
	public void load(Process p, int pageNumber) {
        // Check if the page is loaded, otherwise start the loading
        // process.
        for(PageInfo page : pages.get(p)) {
            if(page.getPageNumber() == pageNumber) {
                return;
            }
        }

        // If the process already exists in the loading page then we've got an error
        if(loadingPages.containsKey(p)) {
            throw new RuntimeException("Process " + p + " attempting to do duplicate loads");
        }

        // Since we're here we know we can start loading the page, ideally we
        // want to use a free space, but if none exists we want to swap out a
        // page. We leave the choice up to the PageReplacer. To do that we need
        // to construct an array of PageInfo objects.
        PageInfo[] processPages = pages.get(p);
        int selectedIndex = replacer.selectNextSwapIndex(processPages);
        processPages[selectedIndex] = null; // Remove the selected item from the array

        loadingPages.put(p, new LoadingPage(pageNumber, pageLoadSpeed, selectedIndex));
	}

	@Override
	public void notifyOfUse(Process p, int pageNumber) {
        for(PageInfo page : pages.get(p)) {
            if(page.getPageNumber() == pageNumber) {
                page.setTimeSinceUse(0);
                return;
            }
        }
	}

    @Override
    public void update() {
    }

    @Override
	public void step() {
        // Update all the current pages.
        for(Map.Entry<Process, PageInfo[]> pageSets : pages.entrySet()) {
            for(PageInfo page : pageSets.getValue()) {
                page.setTimeSinceAdded(page.getTimeSinceAdded() + 1);
            }
        }

        // Update all the loading pages and load them if they're done.
        Iterator<Map.Entry<Process, LoadingPage>> processIterator = loadingPages.entrySet().iterator();
        while(processIterator.hasNext()) {
            Map.Entry<Process, LoadingPage> processes = processIterator.next();
            Process process = processes.getKey();
            LoadingPage loadingPage = processes.getValue();

            loadingPage.loadingTime -= 1;

            if(loadingPage.loadingTime == 0) {
                pages.get(process)[loadingPage.targetMemoryIndex] = new PageInfo(loadingPage.pageNumber, 0, 0);
                processIterator.remove();
            } else if(loadingPage.loadingTime < 0) {
                throw new RuntimeException("Negative page loading time");
            }
        }
	}

    @Override
    public void reset() {
        pages.clear();
        loadingPages.clear();
    }
}
