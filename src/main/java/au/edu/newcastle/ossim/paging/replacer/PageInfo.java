package au.edu.newcastle.ossim.paging.replacer;

public class PageInfo {
    private int pageNumber;
    private int timeSinceAdded;
    private int timeSinceUse;

    public PageInfo(int pageNumber, int timeSinceAdded, int timeSinceUse) {
        this.pageNumber = pageNumber;
        this.timeSinceUse = timeSinceUse;
        this.timeSinceAdded = timeSinceAdded;
    }

    public int getTimeSinceAdded() {
        return this.timeSinceAdded;
    }

    public int getTimeSinceUse() {
        return this.timeSinceUse;
    }

    public void setTimeSinceAdded(int timeSinceAdded) {
        this.timeSinceAdded = timeSinceAdded;
    }

    public void setTimeSinceUse(int timeSinceUse) {
        this.timeSinceUse = timeSinceUse;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }
}
