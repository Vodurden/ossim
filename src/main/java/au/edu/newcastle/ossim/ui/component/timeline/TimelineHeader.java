package au.edu.newcastle.ossim.ui.component.timeline;

import java.awt.*;

public class TimelineHeader extends AbstractTimelineComponent {

	private static final long serialVersionUID = 1L;

    private int m_rows = 0;
    private String m_label = "Time";
    
    public TimelineHeader() {}

    public void setRows(int rows) {
    	m_rows = rows;
    }
    
    @Override
	public int length() {
    	return m_rows;
    }
    
    @Override
    public void paintComponent(Graphics g) {
    	super.paintComponent(g);
		
		// Create a new Graphics2d Instance
		Graphics2D g2 = (Graphics2D)g.create();
	  
		// Get the correct drawing area
		Rectangle drawArea = new Rectangle(
				getInsets().left,
				getInsets().top,
				getWidth() - getInsets().left - getInsets().right,
				getHeight() - getInsets().top - getInsets().bottom
		);
		
		// Find the x-center of the headerr
		int x_center = drawArea.x + ((drawArea.width - drawArea.x) / 2);
		
		// Draw the label for this timeline centered
		FontMetrics metrics = g2.getFontMetrics(g2.getFont());
		int text_center = x_center - (metrics.stringWidth(m_label) / 2);
		g2.drawString(m_label, text_center, drawArea.y + 10);
		
		// Change the font to a monospace font
		g2.setFont(m_font);
		metrics = g2.getFontMetrics(m_font);
		
		int label_height = metrics.getHeight();
		
		// Draw the events on the timeline
		for(Integer time = 1; time <= m_rows; ++time) {
			String text = time.toString();
			int rowTextLength = metrics.stringWidth(text);
			
			Point rowText = new Point(
				x_center - (rowTextLength / 2), // X position to draw the text at
				((drawArea.y + label_height + m_eventGap) + m_eventYPad + m_eventGap) + ((time - 1) * (label_height + m_eventGap * 2))
			);
			
			g2.drawString(text,  rowText.x,  rowText.y);
		}
    }
    
	@Override
	protected Dimension createPreferredSize() {
		// FontMetrics are used to calculate the pixel length
		// of a string
		FontMetrics metrics = getFontMetrics(m_font);
		
		int width = metrics.stringWidth(m_label);
		
		// Now we need to calculate our preferred height
		// which we can calculate by pre-determining the amount of size
		// we will need to render the entire history
        int height = (m_eventYPad * 2 + m_eventGap * 2) + ((m_rows + 1) * (metrics.getHeight() + m_eventGap * 2));

		return new Dimension(width, height);
	}
}
