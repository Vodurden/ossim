package au.edu.newcastle.ossim.ui.component.timeline;

import java.util.Map;

public interface TimelineModel {
	public Map<Integer, String> getHistory();
	public Integer getLatestHistory();
	@Override
	public String toString();
}
