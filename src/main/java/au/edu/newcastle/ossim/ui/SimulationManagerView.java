package au.edu.newcastle.ossim.ui;

import au.edu.newcastle.ossim.SimulationManager;
import au.edu.newcastle.ossim.annotation.AnnotationUtil;
import au.edu.newcastle.ossim.annotation.DisplayName;
import au.edu.newcastle.ossim.loader.FileSystemDataSource;
import au.edu.newcastle.ossim.loader.ProcessDataSource;
import au.edu.newcastle.ossim.ui.component.CheckBoxButtonList;
import au.edu.newcastle.ossim.ui.component.ObjectSelectingCheckBox;
import au.edu.newcastle.ossim.ui.component.event.ObjectSelectedListener;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class SimulationManagerView {
	
	private SimulationManager m_model;

	private JFrame frame = new JFrame("Newcastle University OS Simulation Manager");

    private JList inputFiles = null;
    private JButton btnSelectInputFolder = null;
    private String selctedDirectoryPath = null;

	private CheckBoxButtonList schedulerList = null;
	private CheckBoxButtonList ioDeviceList = null;
	private CheckBoxButtonList ioSchedulerList = null;
	private CheckBoxButtonList pagingDeviceList = null;
    private CheckBoxButtonList pageReplacerList = null;

	private JButton btnRun = null;

    private int numSimulationsToRun = 0;


	/**
	 * Create the frame.
	 */
	public SimulationManagerView(SimulationManager model) {
		this.m_model = model;
		
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 570, 404);
		
		Container pane = frame.getContentPane();
		pane.setLayout(new MigLayout("fill", "[grow][][][][][]", "[grow][]"));

        // Set up the file picker panel:
		Border folderBorder = BorderFactory.createTitledBorder("Data");
		JPanel folderPanel = new JPanel();
		folderPanel.setBorder(folderBorder);
		pane.add(folderPanel, "cell 0 0,grow");

        DefaultListModel inputFilesModel = new DefaultListModel();
        inputFilesModel.addElement("No inputs selected!");
        inputFiles = new JList(inputFilesModel);

		folderPanel.setLayout(new MigLayout("fill", "[grow]", "[grow][]"));
        btnSelectInputFolder = new JButton("Select Input Folder");
        folderPanel.add(inputFiles, "cell 0 0, grow");
        folderPanel.add(btnSelectInputFolder, "cell 0 1");

		// Create the radio button lists
		schedulerList = createBorderedCheckBoxList("Scheduler", m_model.getSchedulers());
		ioDeviceList = createBorderedCheckBoxList("IO Device", m_model.getIODevices());
		ioSchedulerList = createBorderedCheckBoxList("IO Scheduler", m_model.getIOSchedulers());
		pagingDeviceList = createBorderedCheckBoxList("Paging Device", m_model.getPagingDevices());
        pageReplacerList = createBorderedCheckBoxList("Page Replacement", m_model.getPageReplacers());

		// Add the radio button lists to the window
		pane.add(schedulerList, "cell 1 0,grow");
        pane.add(pagingDeviceList, "cell 2 0,grow");
        pane.add(pageReplacerList, "cell 3 0,grow");
		pane.add(ioDeviceList, "cell 4 0,grow");
		pane.add(ioSchedulerList, "cell 5 0,grow");

		btnRun = new JButton("Run (1 simulation will be run)");
		pane.add(btnRun, "cell 0 1,span,grow");
	}
	
	public void updateSimulationsToRun(int numberToRun) {
		String simulationText = numberToRun == 1 ? "simulation" : "simulations";

        numSimulationsToRun = numberToRun;
		btnRun.setText("Run (" + Integer.toString(numSimulationsToRun) + " " + simulationText + " will be run)");
	}

    public int getNumSimulationsToRun() {
        return numSimulationsToRun;
    }
	

	public void display() {
		frame.setVisible(true);
	}

    public File doDirectorySelectDialog() {
        // new File(".") typically corresponds to the current working directory which is
        // where I expect students to typically put their example files.
        JFileChooser fileChooser = new JFileChooser(new File("."));
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = fileChooser.showOpenDialog(frame);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            String selectedPath = fileChooser.getSelectedFile().getAbsolutePath();

            // TODO: Add directory validation
            File selectedDirectory = new File(selectedPath);
            if(selectedDirectory.isDirectory()) {
                return selectedDirectory;
            }
        }

        return null;
    }

    public void selectDirectory(File directory) {
        DefaultListModel inputFilesModel = (DefaultListModel)inputFiles.getModel();
        inputFilesModel.removeAllElements();

        if(directory == null || !directory.isDirectory()) {
            inputFilesModel.addElement("No inputs found!");
            return;
        }

        selctedDirectoryPath = directory.getAbsolutePath();
        for(File child : directory.listFiles()) {
            if(child.isFile()) {
                inputFilesModel.addElement(child.getName());
            }
        }
    }
	
	public ProcessDataSource getDataSource() {
		try {
			return new FileSystemDataSource(selctedDirectoryPath);
		} catch (IOException e) {
			System.err.println("Error retrieving selected file path");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void addRunListener(ActionListener listener) {
		btnRun.addActionListener(listener);
	}

    public void addSelectInputFolderListener(ActionListener listener) {
        btnSelectInputFolder.addActionListener(listener);
    }
	
	public void addSchedulerListener(ObjectSelectedListener listener) {
		schedulerList.addObjectSelectedListener(listener);
	}
	
	public void addIoDeviceListener(ObjectSelectedListener listener) {
		ioDeviceList.addObjectSelectedListener(listener);
	}
	
	public void addIoSchedulerListener(ObjectSelectedListener listener) {
		ioSchedulerList.addObjectSelectedListener(listener);
	}
	
	public void addPagingDeviceListener(ObjectSelectedListener listener) {
		pagingDeviceList.addObjectSelectedListener(listener);
	}

    public void addPageReplacerListener(ObjectSelectedListener listener) {
        pageReplacerList.addObjectSelectedListener(listener);
    }

	/**
	 * Utility function to create a bordered radio
	 * list from the given arguments. This is used to
	 * generate the Scheduler, Process and Instruction lists
	 * 
	 * @return The generated panel
	 */
	private <T> CheckBoxButtonList createBorderedCheckBoxList(String title, List<Class<? extends T>> classes) {
		// Set up the Schedulers panel
		Border schedulerBorder = BorderFactory.createTitledBorder(title);
		CheckBoxButtonList panel = new CheckBoxButtonList();
		panel.setBorder(schedulerBorder);
		
		for(Class<? extends T> clazz : classes) {
			DisplayName annotation = AnnotationUtil.getAnnotationFromSubclass(clazz, DisplayName.class);
			String name = clazz.getSimpleName();
			if(annotation != null) {
				name = annotation.value();
			}
			
			// Add a radio button for each name
			ObjectSelectingCheckBox checkBox = new ObjectSelectingCheckBox(name);
			checkBox.setSelectedObject(clazz);
			panel.add(checkBox);
			System.out.println("Found clazz with name: " + name);
		}	
		
		return panel;
	}
}
