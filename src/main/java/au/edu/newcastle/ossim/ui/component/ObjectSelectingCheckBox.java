package au.edu.newcastle.ossim.ui.component;

import au.edu.newcastle.ossim.ui.component.event.ObjectSelectedEvent;
import au.edu.newcastle.ossim.ui.component.event.ObjectSelectedListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

public class ObjectSelectingCheckBox extends JCheckBox implements ItemListener {

	private static final long serialVersionUID = 1L;
	
	private List<ObjectSelectedListener> m_objectListeners = new ArrayList<ObjectSelectedListener>();
	private Object m_selectedObject;

	public ObjectSelectingCheckBox() {
		super();
		
		Initialize();
	}
	
	public ObjectSelectingCheckBox(Action a) {
		super(a);
		
		Initialize();
	}
	
	public ObjectSelectingCheckBox(Icon icon) {
		super(icon);
		
		Initialize();
	}
	
	public ObjectSelectingCheckBox(Icon icon, boolean selected) {
		super(icon, selected);
		
		Initialize();
	}
	
	public ObjectSelectingCheckBox(String text) {
		super(text);
		
		Initialize();
	}
	
	public ObjectSelectingCheckBox(String text, boolean selected) {
		super(text, selected);
		
		Initialize();
	}
	
	public ObjectSelectingCheckBox(String text, Icon icon) {
		super(text, icon);
		
		Initialize();
	}
	
	public ObjectSelectingCheckBox(String text, Icon icon, boolean selected) {
		super(text, icon, selected);
		
		Initialize();
	}
	
	private void Initialize() {
		addItemListener(this);
	}
	
	public void addObjectSelectedListener(ObjectSelectedListener listener) {
		m_objectListeners.add(listener);
	}

    public List<ObjectSelectedListener> getObjectSelectedListeners() {
        return m_objectListeners;
    }
	
	public void setSelectedObject(Object o) {
		m_selectedObject = o;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		for(ObjectSelectedListener listener : m_objectListeners) {
			ObjectSelectedEvent event = new ObjectSelectedEvent(
					(ItemSelectable)e.getSource(), 
					e.getID(), 
					e.getItem(), 
					e.getStateChange(), 
					m_selectedObject);
			listener.objectSelected(event);
		}
	}

}
