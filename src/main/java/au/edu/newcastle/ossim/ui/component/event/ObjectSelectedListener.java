package au.edu.newcastle.ossim.ui.component.event;

public interface ObjectSelectedListener {
	void objectSelected(ObjectSelectedEvent e);
}
