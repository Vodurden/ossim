package au.edu.newcastle.ossim.ui.component.event;

import java.awt.*;
import java.awt.event.ItemEvent;

public class ObjectSelectedEvent extends ItemEvent {

	private static final long serialVersionUID = 1L;
	
	private Object m_selectedItem;

	public ObjectSelectedEvent(ItemSelectable source, int id, Object item, int stateChange, Object selectedItem) {
		super(source, id, item, stateChange);
		
		m_selectedItem = selectedItem;
	}
	
	public Object getSelectedItem() { return m_selectedItem; }
	public void setSelectedItem(Object item) { m_selectedItem = item; }
}
