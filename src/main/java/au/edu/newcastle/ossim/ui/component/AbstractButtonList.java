package au.edu.newcastle.ossim.ui.component;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

public class AbstractButtonList <T extends AbstractButton> extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private ButtonGroup m_buttonGroup = new ButtonGroup();
	private List<T> m_buttons = new ArrayList<T>();
	
	private boolean m_groupButtons;
	
	protected List<T> getButtons() {
		return m_buttons;
	}

	public AbstractButtonList(boolean groupButtons) {
		setLayout(new MigLayout("flowy", "[]", "[]"));
		
		m_groupButtons = groupButtons;
	}
	
	public void add(T button) {
		super.add(button);
		
		if(m_groupButtons) {
			m_buttonGroup.add(button);
		}
		m_buttons.add(button);
	}

	public void addActionListener(ActionListener listener) {
		for(T button : m_buttons) {
			button.addActionListener(listener);
		}
	}
	
	public void addItemListener(ItemListener listener) {
		for(T button : m_buttons) {
			button.addItemListener(listener);
		}
	}
	
	public void addChangeListener(ChangeListener listener) {
		for(T button : m_buttons) {
			button.addChangeListener(listener);
		}
	}
}
