package au.edu.newcastle.ossim.ui.component;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.util.ArrayList;
import java.util.List;

public class ArrayModel<T> implements ListModel {
	
	private List<ListDataListener> m_listeners = new ArrayList<ListDataListener>();
	private List<T> m_data = new ArrayList<T>();
	
	@Override
	public void addListDataListener(ListDataListener listener) {
		m_listeners.add(listener);
	}
	
	@Override
	public void removeListDataListener(ListDataListener listener) {
		m_listeners.remove(listener);
	}

	@Override
	public T getElementAt(int index) {
		if(!m_data.isEmpty() && index >= 0 && index < m_data.size()) {
			return m_data.get(index);
		} else {
			return null;
		}
	}

	@Override
	public int getSize() {
		return m_data.size();
	}
	
	public void add(T object) {
		m_data.add(object);
	}

}
