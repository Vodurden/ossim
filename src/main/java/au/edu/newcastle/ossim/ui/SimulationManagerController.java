package au.edu.newcastle.ossim.ui;

import au.edu.newcastle.ossim.Simulation;
import au.edu.newcastle.ossim.SimulationManager;
import au.edu.newcastle.ossim.SimulationSet;
import au.edu.newcastle.ossim.cpu.scheduler.ProcessScheduler;
import au.edu.newcastle.ossim.io.device.IODevice;
import au.edu.newcastle.ossim.io.scheduler.IOScheduler;
import au.edu.newcastle.ossim.loader.ProcessDataSource;
import au.edu.newcastle.ossim.paging.device.PagingDevice;
import au.edu.newcastle.ossim.paging.replacer.PageReplacer;
import au.edu.newcastle.ossim.ui.component.event.ObjectSelectedEvent;
import au.edu.newcastle.ossim.ui.component.event.ObjectSelectedListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.File;

public class SimulationManagerController {

	private SimulationManager m_model;
	private SimulationManagerView m_view;
	
	public SimulationManagerController(SimulationManager model, SimulationManagerView view) {
		this.m_model = model;
		this.m_view = view;
		
		m_view.addRunListener(new RunListener());
        m_view.addSelectInputFolderListener(new SelectInputFolderListener());
		m_view.addSchedulerListener(new SchedulerListener());
		m_view.addIoDeviceListener(new IODeviceListener());
        m_view.addIoSchedulerListener(new IOSchedulerListener());
		m_view.addPagingDeviceListener(new PagingDeviceListener());
        m_view.addPageReplacerListener(new PageReplacerListener());
	}
	
	public void start() {
		m_view.display();
	}
	
	private void update() {
		m_view.updateSimulationsToRun(m_model.getNumberOfSimulationToRun());
	}
	
	/**
	 * Inner Class Event Handlers
	 */
	class RunListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// Run has been clicked, we need to spawn
			// a new simulation with the given parameters
			// in a new thread
			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
                    // We only want to run a SimulationSet if there's more then one
                    // process to run.
                    SimulationSet model = m_model.buildSimulationSet(m_view.getDataSource());
                    SimulationSetView view = new SimulationSetView(model);
                    SimulationSetController controller = new SimulationSetController(model, view);
                    controller.start();
                }
            });
		}
	}

    class SelectInputFolderListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            File selectedDirectory = m_view.doDirectorySelectDialog();
            m_view.selectDirectory(selectedDirectory);
        }
    }
	
	class SchedulerListener extends CheckBoxListener<ProcessScheduler> {
		@Override
		protected void doCall(Class<? extends ProcessScheduler> arg, boolean selected) {
			m_model.selectScheduler(arg, selected);
			
			update();
		}
	}
	
	class IODeviceListener extends CheckBoxListener<IODevice> {
		@Override
		protected void doCall(Class<? extends IODevice> arg, boolean selected) {
			m_model.selectIODevice(arg, selected);
			
			update();
		}
	}
	
	class IOSchedulerListener extends CheckBoxListener<IOScheduler> {
		@Override
		protected void doCall(Class<? extends IOScheduler> arg, boolean selected) {
			m_model.selectIOScheduler(arg, selected);
			
			update();
		}
	}
	
	class PagingDeviceListener extends CheckBoxListener<PagingDevice> {
		@Override
		protected void doCall(Class<? extends PagingDevice> arg, boolean selected) {
			m_model.selectPagingDevice(arg, selected);
			
			update();
		}
	}

    class PageReplacerListener extends CheckBoxListener<PageReplacer> {
        @Override
        protected void doCall(Class<? extends PageReplacer> arg, boolean selected) {
            m_model.selectPageReplacer(arg, selected);

            update();
        }
    }
	
	/**
	 * This is a helper class to allow us to easily bind the class
	 * selection of a RadioButtonList to an actual call on the model.
	 * The basic idea is that any RadioButtonList on changing will want to call
	 * m_model.setSomething(class), to easily bind this we can extend the RadioButtonList
	 * with type <T> 
	 * 
	 * @author Jake Woods
	 *
	 * @param <T>
	 */
	private abstract class CheckBoxListener <T> implements ObjectSelectedListener {
		@Override
		@SuppressWarnings("unchecked")
		public void objectSelected(ObjectSelectedEvent e) {
			Class<? extends T> clazz;
			clazz = (Class<? extends T>)e.getSelectedItem();
			
			boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
			doCall(clazz, selected);
		}
		
		protected abstract void doCall(Class<? extends T> arg, boolean checked);
	}
}
