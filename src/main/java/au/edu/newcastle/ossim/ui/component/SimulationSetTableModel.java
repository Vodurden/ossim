package au.edu.newcastle.ossim.ui.component;

import au.edu.newcastle.ossim.Simulation;
import au.edu.newcastle.ossim.annotation.AnnotationUtil;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class SimulationSetTableModel extends AbstractTableModel {
	
	private class Column {
		public String name;
		public Class columnClass;
		
		public Column(String name, Class columnClass) {
			this.name = name;
			this.columnClass = columnClass;
		}
	}
	
	private List<Simulation> m_simulations;
	private List<Column> m_columns = new ArrayList<Column>();
	private int m_rowCount = 0;
	private int m_columnCount;
	
	public SimulationSetTableModel(List<Simulation> simulations) {
		m_columns.add(new Column("Scheduler", String.class));
		m_columns.add(new Column("Paging Device", String.class));
        m_columns.add(new Column("Page Replacer", String.class));
		m_columns.add(new Column("IO Device", String.class));
        m_columns.add(new Column("IO Scheduler", String.class));
		m_columns.add(new Column("Time spent waiting", Integer.class));
		m_columns.add(new Column("Processing time", Integer.class));
		
		m_simulations = simulations;
		
		update();
	}

	public void update() {
		m_columnCount = m_columns.size();
		m_rowCount = m_simulations.size();
	}
	
	@Override
	public int getColumnCount() {
		return m_columnCount;
	}

	@Override
	public int getRowCount() {
		return m_rowCount;
	}

	@Override
	public Object getValueAt(int row, int col) {
		Simulation sim = m_simulations.get(row);
		switch(col) {
		case 0: return AnnotationUtil.getDisplayName(sim.getScheduler().getClass());
		case 1: return AnnotationUtil.getDisplayName(sim.getPagingDevice().getClass());
        case 2: return AnnotationUtil.getDisplayName(sim.getPageReplacer().getClass());
		case 3: return AnnotationUtil.getDisplayName(sim.getIODevice().getClass());
        case 4: return AnnotationUtil.getDisplayName(sim.getIOScheduler().getClass());
		case 5: return sim.getTotalTimeSpentWaiting();
		case 6: return sim.getTotalTimeToComplete();
		}
		return null;
	}
	
	@Override
	public Class getColumnClass(int c) {
		return m_columns.get(c).columnClass;
	}
	
	@Override
	public String getColumnName(int c) {
		return m_columns.get(c).name;
	}
	
}
