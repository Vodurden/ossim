package au.edu.newcastle.ossim.ui;

import au.edu.newcastle.ossim.Simulation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimulationController {

	private Simulation m_model;
	private SimulationView m_view;

	public SimulationController(Simulation model, SimulationView view) {
		this.m_model = model;
		this.m_view = view;
		
		m_view.addStepListener(new StepListener());
        m_view.addStepToEndListener(new StepToEndListener());
        m_view.addResetListener(new ResetListener());
	}
	
	
	public void start() {
		m_view.display();
	}

    private abstract class ListModifyingListener implements ActionListener {

        @Override
        public final void actionPerformed(ActionEvent e) {
            if(doAction()) {
                m_view.getReadyList().setListData(m_model.getReadyList().toArray());
                m_view.getBlockedList().setListData(m_model.getBlockedList().toArray());
                m_view.getFinishedList().setListData(m_model.getFinishedList().toArray());

                m_view.update();
            }
        }

        protected abstract boolean doAction();
    }



    /**
	 * Inner Class Event Handlers
	 * 
	 * TODO: Document Better
	 */
	class StepListener extends ListModifyingListener {
        @Override
        protected boolean doAction() {
            if(m_model.finished()) {
                return false;
            }

            m_model.step();
            return true;
        }
	}

    class StepToEndListener extends ListModifyingListener {
        @Override
        public boolean doAction() {
            if(m_model.finished()) {
                return false;
            }

            m_model.runToEnd();
            return true;
        }
    }

    class ResetListener extends ListModifyingListener {
        @Override
        public boolean doAction() {
            m_model.reset();
            m_view.reset();
            return true;
        }
    }
}
