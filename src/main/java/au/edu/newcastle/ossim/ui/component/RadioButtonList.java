package au.edu.newcastle.ossim.ui.component;

import javax.swing.*;

public class RadioButtonList extends AbstractButtonList<JRadioButton> {
	public RadioButtonList() {
		super(true);
	}

	private static final long serialVersionUID = 2L;
}
