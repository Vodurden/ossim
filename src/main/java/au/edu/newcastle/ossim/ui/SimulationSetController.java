package au.edu.newcastle.ossim.ui;

import au.edu.newcastle.ossim.Simulation;
import au.edu.newcastle.ossim.SimulationSet;

import javax.swing.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

public class SimulationSetController {
	private SimulationSetView m_view;

    private Map<Simulation, SimulationView> m_openedSimulationViews = new HashMap<Simulation, SimulationView>();
	
	public SimulationSetController(SimulationSet model, SimulationSetView view) {
		m_view = view;
		
		m_view.addMouseListener(new RowDoubleClickedListener());
        m_view.addOpenSimulationListener(new OpenSimulationClickedListener());
        m_view.addWindowListener(new OnWindowClosingListener());
	}
	
	public void start() {
		m_view.display();
	}

    private void buildSelectedSimulation() {
        // We want to build a simulation view for the selected simulation.
        Simulation model = m_view.getSelectedSimulation();
        if(model == null) { return; }

        if(m_openedSimulationViews.containsKey(model)) {
            // If we're here then an existing simulation is already open. Let's bring
            // that into focus instead.
            m_openedSimulationViews.get(model).focus();
        } else {
            SimulationView view = new SimulationView(model);
            SimulationController controller = new SimulationController(model, view);

            m_openedSimulationViews.put(model, view);

            controller.start();
        }
    }

    class OnWindowClosingListener implements WindowListener {
        @Override
        public void windowClosing(WindowEvent e) {
            // If we close then we want to close all of our child simulations.
            for(SimulationView childView : m_openedSimulationViews.values()) {
                // If a view is closed by the user close() still doesn't appear to cause an issue
                // even though the frame is disposed. I've tested this allowing for long intervals
                // so that the JVM has the opportunity to clear the memory but it appears to work fine.
                childView.close();
            }
        }

        @Override
        public void windowOpened(WindowEvent e) {
        }

        @Override
        public void windowClosed(WindowEvent e) {
        }

        @Override
        public void windowIconified(WindowEvent e) {
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
        }

        @Override
        public void windowActivated(WindowEvent e) {
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
        }
    }

    class OpenSimulationClickedListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                buildSelectedSimulation();
            } catch(IndexOutOfBoundsException ex) {
                // Clicked without selection, the user is going to expect something
                // so lets give them an error.
                JOptionPane.showMessageDialog(null, "No simulation selected, please select a simulation", "Error!", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }
	
	class RowDoubleClickedListener implements MouseListener {

		@Override
		public void mousePressed(MouseEvent e) {
			if(e.getClickCount() == 2) { // Double click
                try {
                    buildSelectedSimulation();
                } catch(IndexOutOfBoundsException ex) {
                    // Purposefully ignore, it means we double clicked somewhere
                    // not on a selected item. Swallow the error
                }
			}
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}

		@Override
		public void mouseReleased(MouseEvent e) {}
	}
}
