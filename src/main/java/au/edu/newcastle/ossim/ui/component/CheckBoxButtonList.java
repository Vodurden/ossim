package au.edu.newcastle.ossim.ui.component;

import au.edu.newcastle.ossim.ui.component.event.ObjectSelectedListener;

public class CheckBoxButtonList extends AbstractButtonList<ObjectSelectingCheckBox> {
	public CheckBoxButtonList() {
		super(false);
	}

	private static final long serialVersionUID = 1L;
	
	public void addObjectSelectedListener(ObjectSelectedListener listener) {
		for(ObjectSelectingCheckBox checkBox : getButtons())  {
			checkBox.addObjectSelectedListener(listener);
		}
	}
}
