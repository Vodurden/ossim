package au.edu.newcastle.ossim.ui.component.timeline;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class is responsible for painting a single
 * timeline. This is represented as a labeled line that
 * has a series of square "events" below it.
 * <p>
 * Time is assumed to be an integer based step
 * timeline, events that are drawn at time n in one
 * timeline will be placed in the same position relative
 * to the label on all timelines.
 * 
 * @author Jake Woods 
 */
public class Timeline extends AbstractTimelineComponent {

	private static final long serialVersionUID = 1L;
	
	// History stuff
	private int m_previousHistorySize = 0; // When this changes we need to update our preferred size
	
	private TimelineModel m_model;

	public Timeline(TimelineModel model) {
		m_model = model;
	}

	@Override
	public int length() {
		return m_model.getLatestHistory();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		if(m_model.getHistory().size() != m_previousHistorySize) {
			m_previousHistorySize = m_model.getHistory().size();
		}
		
		// Determine the drawing position
		super.paintComponent(g);
		
		
		// Create a new Graphics2d Instance
		Graphics2D g2 = (Graphics2D)g.create();
	  
		// Get the correct drawing area
		Rectangle drawArea = new Rectangle(
				getInsets().left,
				getInsets().top,
				getWidth() - getInsets().left - getInsets().right,
				getHeight() - getInsets().top - getInsets().bottom
		);
		
		// Find the x-center of the timeline
		int x_center = drawArea.x + ((drawArea.width - drawArea.x) / 2);
		
		// Draw the label for this timeline centered
		FontMetrics metrics = g2.getFontMetrics(g2.getFont());
		int text_center = x_center - (metrics.stringWidth(m_model.toString()) / 2);
		int label_height = metrics.getHeight();
		g2.drawString(m_model.toString(), text_center, drawArea.y + 10);
		
		// Find the top and bottom of the timeline
		Point timelineTop = new Point( x_center, drawArea.y + label_height + m_eventGap);
		Point timelineBottom = new Point( x_center, drawArea.y + getPreferredSize().height);
		
		// Draw the timeline itself
		g2.drawLine(timelineTop.x, timelineTop.y, timelineBottom.x, timelineBottom.y);
		
		// Change the font to a monospace font
		g2.setFont(m_font);
		metrics = g2.getFontMetrics(m_font);
		
		label_height = metrics.getHeight();
		
		// Find the largest event length
		int boxWidth = m_minimumWidth;
		for(String event : m_model.getHistory().values()) {
			int challenger = metrics.stringWidth(event);
			if(challenger > boxWidth ) {
				boxWidth = challenger;
			}
		}
		
		// Used to store the connections for rendering after
		// we need to render connections after so they render over
		// the event borders
		List<Integer> connections = new ArrayList<Integer>();
		
		// Draw the events on the timeline
		for(Map.Entry<Integer, String> entry : m_model.getHistory().entrySet()) {
			// Time starts at 1 
			Integer index = entry.getKey();
			String event = entry.getValue();
			
			if(!event.equals("")) {
				// Drawing for new events
				Rectangle eventBorder = new Rectangle(0, 0, metrics.stringWidth(event) + (m_eventXPad * 2), label_height + (m_eventYPad * 2));
				int eventTextLength = metrics.stringWidth(event);
				
				Point eventText = new Point(
					x_center - (eventTextLength / 2), // X position to draw the text at
					(timelineTop.y + m_eventYPad + m_eventGap) + (index * (label_height + m_eventGap * 2))
				);
				
				// Center the event border on the text position
				eventBorder.setLocation(x_center - (eventBorder.width / 2), eventText.y - (label_height) + (m_eventYPad));
				
				g2.setPaint(Color.WHITE);
				g2.fill(eventBorder);
				g2.setPaint(Color.BLACK);
				g2.draw(eventBorder);
				
				// Draw the event itself
				g2.drawString(event, eventText.x, eventText.y);
			} else {
				connections.add(index);
			}
			
		}
		
		for(Integer index : connections) {
			// Drawing for continuation events
			Rectangle eventLine = new Rectangle(0, 0, m_connectionWidth, label_height + (m_eventYPad * 2) + (m_eventGap * 2) + m_eventYPad + 1);
			
			eventLine.setLocation(
				x_center - (eventLine.width / 2), 
				(timelineTop.y + m_eventYPad + m_eventGap) + (index * (label_height + m_eventGap * 2)) - (label_height + (m_eventYPad * 2))  
			);
			
			g2.setPaint(Color.WHITE);
			g2.fill(eventLine);
			g2.setPaint(Color.BLACK);
			g2.drawLine(eventLine.x, eventLine.y, eventLine.x, eventLine.y + eventLine.height - 1);
			g2.drawLine(eventLine.x + eventLine.width, eventLine.y, eventLine.x + eventLine.width, eventLine.y + eventLine.height - 1);
		}
	}
	
	@Override
	protected Dimension createPreferredSize() {
		// FontMetrics are used to calculate the pixel length
		// of a string
		FontMetrics metrics = getFontMetrics(m_font);
		
		// Find the longest word out of the events and label
		// to calculate the preferred width
		String longest = m_model.toString();
		for(String event : m_model.getHistory().values()) {
			if(event.length() > longest.length()) {
				longest = event;
			} else if(event.length() == longest.length()){
				// Here we need to do some special processing because
				// the pixel length of the string may be different compared
				// to the character length so we need to make sure
				// we choose the actual longest
				// NOTE: If you haven't seen this syntax before, this is a ternary-if condition
				// essentially it's a short if statement, the syntax is:
				//
				// 		condition ? do-if-true : do-if-false
				//
				// so in our case it's equivalent to:
				//
				//		if(metrics.stringWidth(event) > metrics.stringWidth(longest)) {
				//			longest = event;	
				//		} else {
				//			longest = longest;
				//		}
				longest = metrics.stringWidth(event) > metrics.stringWidth(longest) ? event : longest;
			}
		}
		
		// Determine the width of the string using the current font 
		int width = metrics.stringWidth(longest) + (m_eventXPad * 2) + (m_eventXMargin * 2);
		
		// Now we need to calculate our preferred height
		// which we can calculate by pre-determining the amount of size
		// we will need to render the entire history
		int height = (m_eventYPad * 2 + m_eventGap * 2) + ((m_model.getLatestHistory() + 1) * (metrics.getHeight() + m_eventGap * 2));

		return new Dimension(width, height);
	}
}
