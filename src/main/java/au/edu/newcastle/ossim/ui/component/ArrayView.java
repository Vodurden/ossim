package au.edu.newcastle.ossim.ui.component;

import javax.swing.*;
import java.awt.*;

public class ArrayView extends JComponent {
	
	  @Override
	  protected void paintComponent(Graphics graphics) {
		  super.paintComponent(graphics);
		  
		  // Create a new Graphics2d Instance
		  Graphics2D g2 = (Graphics2D)graphics.create();
		  
		  // Get the correct drawing area
		  int x = getInsets().left;
		  int y = getInsets().top;
		  int w = getWidth() - getInsets().right;
		  int h = getHeight() - getInsets().bottom;
		  
		  if(isOpaque()) {
			  g2.setBackground(getBackground());
			  g2.fillRect(0, 0, getWidth(), getHeight());
		  }
		  
		  g2.setPaint(Color.BLUE);
		  g2.fillOval(x, y, w, h);
	  }
}
