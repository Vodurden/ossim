package au.edu.newcastle.ossim.ui;

import au.edu.newcastle.ossim.Simulation;
import au.edu.newcastle.ossim.cpu.execution.Process;
import au.edu.newcastle.ossim.ui.component.timeline.ParallelTimeline;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SimulationView {

	private Simulation model;
	
	private JFrame m_frame = new JFrame("Newcastle University OS Simulator");
	private SimulationToolbar m_toolbar = new SimulationToolbar();
	private JList m_readyList; 
	private JList m_blockedList;
	private JList m_finishedList;
	
	private ParallelTimeline timeline;

	/**
	 * Create the application.
	 */
	public SimulationView(Simulation model) {
		this.model = model;

        m_frame.getContentPane().setLayout(new MigLayout("fill", "[]", "[]"));

        Container pane = m_frame.getContentPane();
        pane.add(m_toolbar, "dock north");

        // Set up the scrollpane
        JPanel mainArea = new JPanel(new MigLayout("fill", "[][]", "[]"));
        JScrollPane scrollPane = new JScrollPane(mainArea);
        pane.add(scrollPane);

		// Set up the lists
		m_readyList = new JList(this.model.getReadyList().toArray());
		m_blockedList = new JList(this.model.getBlockedList().toArray());
		m_finishedList = new JList(this.model.getFinishedList().toArray());

		JLabel readyLabel = new JLabel("Ready Queue");
		JLabel blockedLabel = new JLabel("Blocked Queue");
		JLabel finishedLabel = new JLabel("Finished");
		
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new MigLayout("flowy"));
		leftPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		leftPanel.add(readyLabel);
		leftPanel.add(m_readyList);
		leftPanel.add(blockedLabel);
		leftPanel.add(m_blockedList);
		leftPanel.add(finishedLabel);
		leftPanel.add(m_finishedList);
		
		timeline = new ParallelTimeline();
		
		// Why can't you just have LINQ java. 
		// Sort the list by IDs so they're in the right order.
		// We can't assume that getAllProcesses returns a sorted list.
		List<Process> processes = this.model.getAllProcesses();
		Collections.sort(processes, new Comparator<Process>() {
			public int compare(final Process lhs, final Process rhs) {
				return lhs.getID().compareTo(rhs.getID());
			}
		});
		
		for(Process p : processes) {
			timeline.add(p);
		}

        // Add the IO Timelines
        timeline.add(this.model.getIOCurrentRequestHistorian());
        timeline.add(this.model.getIORequestQueueHistorian());

		timeline.setBorder(BorderFactory.createLineBorder(Color.red));

		mainArea.add(timeline, "dock west");
		mainArea.add(leftPanel, "dock west");
		
		initialize();
        update();
	}
	
	public JList getReadyList() {
		return m_readyList;
	}
	
	public JList getBlockedList() {
		return m_blockedList;
	}
	
	public JList getFinishedList() {
		return m_finishedList;
	}
	
	public void display() {
		m_frame.setVisible(true);
	}
	
	public void update() {
		timeline.update();
	}

    public void reset() {
        timeline.reset();
    }

    public void focus() {
        m_frame.toFront();
        m_frame.repaint();
    }

    public void close() {
        m_frame.dispatchEvent(new WindowEvent(m_frame, WindowEvent.WINDOW_CLOSING));
    }

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		m_frame.setBounds(100, 100, 450, 300);
		m_frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void addStepListener(ActionListener listener) {
		m_toolbar.addStepListener(listener);
	}

    public void addStepToEndListener(ActionListener listener) {
        m_toolbar.addStepToEndListener(listener);
    }

    public void addResetListener(ActionListener listener) {
        m_toolbar.addResetListener(listener);
    }
}
