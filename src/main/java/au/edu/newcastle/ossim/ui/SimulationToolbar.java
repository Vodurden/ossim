package au.edu.newcastle.ossim.ui;

import javax.swing.*;
import java.awt.event.ActionListener;

public class SimulationToolbar extends JToolBar {

	private static final long serialVersionUID = 1L;
	
	private JButton step;
    private JButton stepToEnd;
    private JButton reset;
	
	public SimulationToolbar() {
		this.setFloatable(false); // Don't let users drag the toolbar
		
		step = new JButton("Step");
        stepToEnd = new JButton("Step to End");
        reset = new JButton("Reset");

		this.add(step);
        this.add(stepToEnd);
        this.add(reset);
	}

	public void addStepListener(ActionListener listener) {
		step.addActionListener(listener);
	}

    public void addStepToEndListener(ActionListener listener) {
        stepToEnd.addActionListener(listener);
    }

    public void addResetListener(ActionListener listener) {
        reset.addActionListener(listener);
    }
}
