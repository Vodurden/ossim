package au.edu.newcastle.ossim.ui.component.timeline;

import au.edu.newcastle.ossim.ui.component.timeline.AbstractTimelineComponent;
import au.edu.newcastle.ossim.ui.component.timeline.Timeline;
import au.edu.newcastle.ossim.ui.component.timeline.TimelineHeader;
import au.edu.newcastle.ossim.ui.component.timeline.TimelineModel;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;

public class ParallelTimeline extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private List<AbstractTimelineComponent> m_timelines = new ArrayList<AbstractTimelineComponent>();
	private TimelineHeader m_header = new TimelineHeader();
	
	public ParallelTimeline() {
		this.setLayout(new MigLayout("flowx"));
		this.addComponentListener(new ResizeListener());
		
		add(m_header);
	}

	public void add(AbstractTimelineComponent component) {
		m_timelines.add(component);
		
		add(component, "gap 10px 10px");
		recalculatePreferredSize();
	}
	
	public void add(TimelineModel timelineModel) {
		Timeline timeline = new Timeline(timelineModel);
		m_timelines.add(timeline);
		
		add(timeline, "gap 5px 5px");
		recalculatePreferredSize();
	}

    public void reset() {
        m_header.setRows(0);
        recalculatePreferredSize();
    }
	
	public void update() {
		// Find the longest timeline and set the header's number length to it
		int longest = 0;
		for(AbstractTimelineComponent t : m_timelines) {
            t.updatePreferredSize();
			if(t.length() > longest) {
				longest = t.length();
			}
		}
		m_header.setRows(longest + 1);
		
		recalculatePreferredSize();
		revalidate();
		repaint();
	}
	
	private void recalculatePreferredSize() {
		// The size of the timelines will have changed, find the largest
		// and make them all the same size
		Dimension largest = new Dimension(0, 0);
		for(AbstractTimelineComponent t : m_timelines) {
			if(t.getPreferredSize().height > largest.height) {
				largest = t.getPreferredSize();
			}
		}
		
		for(AbstractTimelineComponent t : m_timelines) {
			Dimension current = t.getPreferredSize();
			current.height = largest.height;
			t.setPreferredSize(current);
		}
	}
	
	private class ResizeListener implements ComponentListener {
		@Override
		public void componentResized(ComponentEvent arg0) {
			recalculatePreferredSize();
		}
		
		@Override
		public void componentHidden(ComponentEvent arg0) {}
		@Override
		public void componentMoved(ComponentEvent arg0) {}
		@Override
		public void componentShown(ComponentEvent arg0) {}
	}
	
}
