package au.edu.newcastle.ossim.ui;

import au.edu.newcastle.ossim.Simulation;
import au.edu.newcastle.ossim.SimulationSet;
import au.edu.newcastle.ossim.ui.component.SimulationSetTableModel;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;

public class SimulationSetView {
	private SimulationSet m_model;
	
	private JFrame m_frame = new JFrame("Simulation Set");
	private JTable m_table;

    private JButton m_openSimulation = new JButton("Open Simulation");
	
	public SimulationSetView(SimulationSet model) {
		m_model = model;
		
		// Configure the frame and layout
		m_frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		m_frame.setBounds(100, 100, 570, 404);
		
		Container pane = m_frame.getContentPane();
		pane.setLayout(new MigLayout("fill", "[grow,fill]", "[grow,fill][]"));
		
		// Configure the table
		SimulationSetTableModel tableModel = new SimulationSetTableModel(m_model.getSimulations());
		m_table = new JTable(tableModel);
		
		JScrollPane scrollPane = new JScrollPane(m_table);
		m_table.setFillsViewportHeight(true);
		
		m_frame.add(scrollPane, "cell 0 0");
        m_frame.add(m_openSimulation, "cell 0 1");
	}
	
	public void display() {
		m_frame.setVisible(true);
	}
	
	public Simulation getSelectedSimulation() {
		return m_model.getSimulation(m_table.getSelectedRow());
	}
	
	// Event binders
	public void addMouseListener(MouseListener listener) {
		m_table.addMouseListener(listener);
	}

    public void addOpenSimulationListener(ActionListener listener) {
       m_openSimulation.addActionListener(listener);
    }

    public void addWindowListener(WindowListener listener) {
        m_frame.addWindowListener(listener);
    }
}
