package au.edu.newcastle.ossim.ui;

import au.edu.newcastle.ossim.SimulationManager;

import java.awt.*;

public class SimulationUI {
	
	private SimulationManager m_manager;
	
	public SimulationUI(SimulationManager simulation) {
		m_manager = simulation;
	}
	
	public void show() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	private void createAndShowGUI() {
		try {
			SimulationManagerView window = new SimulationManagerView(m_manager);
			SimulationManagerController controller = new SimulationManagerController(m_manager, window);
			
			controller.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
