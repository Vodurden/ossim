package au.edu.newcastle.ossim.ui.component.timeline;

import javax.swing.*;
import java.awt.*;

/**
 * Utility class for variables and functionality
 * that is shared between Timeline and Timeline header,
 * also allows for multiple headers in a ParallelTimeline 
 * 
 * @author Jake Woodss
 */
public abstract class AbstractTimelineComponent extends JComponent {
	private static final long serialVersionUID = 1L;
	
	private Dimension m_preferredSize = null;

	protected Font m_font = new Font( "Monospaced", Font.PLAIN, 12 );
	
	// Size stuff
	protected int m_eventGap = 5; // The gap between events vertically in pixels
	protected int m_eventXPad = 6; // The number of pixels to pad on the X axis
	protected int m_eventYPad = 2; // The number of pixels to pad a border around events
	protected int m_eventXMargin = 2; // The area outside the borders on the X axis of each event
	protected int m_minimumWidth = 2;	
	protected int m_connectionWidth = 8;
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

	abstract protected Dimension createPreferredSize();
	
	public void updatePreferredSize() {
		m_preferredSize = createPreferredSize();
	}
	
	@Override
	public Dimension getPreferredSize() {
		if(m_preferredSize == null) {
			m_preferredSize = createPreferredSize();
		}
		
		return m_preferredSize;
	}
	
	@Override
	public void setPreferredSize(Dimension d) {
		m_preferredSize = d;
	}
	
	/**
	 * @return The number of items in the timeline component
	 */
	abstract public int length();
}
