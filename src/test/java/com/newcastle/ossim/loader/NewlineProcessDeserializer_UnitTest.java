package com.newcastle.ossim.loader;

import au.edu.newcastle.ossim.cpu.execution.Instruction;
import au.edu.newcastle.ossim.loader.InstructionDeserializer;
import au.edu.newcastle.ossim.loader.NewlineProcessDeserializer;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class NewlineProcessDeserializer_UnitTest {
	
	private InstructionDeserializer instructionDeserializer;
	private NewlineProcessDeserializer processDeserializer;
	
	@Before
	public void setUp() {
		/* You can read more about mock classes at http://code.google.com/p/mockito/
		 * Essentially this creates a stub objects that we can then inject
		 * stub methods in to to test things.
		 * 
		 * This is useful so that if our dependencies such as InstructionDeserializer
		 * are broken, these tests will still work as we've mocked their behavior.
		 */
		instructionDeserializer = mock(InstructionDeserializer.class);
		
		// Return a mocked instruction whenever any instruction.deserialize() function is called
		Instruction instruction = mock(Instruction.class);
		when(instructionDeserializer.deserialize(any(String.class))).thenReturn(instruction);
		
		processDeserializer = new NewlineProcessDeserializer(instructionDeserializer);
	}

	@Test
	public void StringIsSplitUpByNewline() {
		String input = 
			"C, 1\n"
			+ "C, 2\n"
			+ "I, 50\n"
			+ "C, 5\n"
			+ "C, 10\n";
		
		List<Instruction> output = processDeserializer.deserialize(input);
		
		// Ensure that we've created 5 instructions from our five
		// input lines
		assertTrue(output.size() == 5);
	}

	@Test
	public void CorrectlyHandlesWindowsLineFormat() {
		String windowsInput = 
			"C, 1\r\n"
			+ "C, 2\r\n"
			+ "I, 50\r\n"
			+ "C, 5\r\n"
			+ "C, 10\r\n";
		
		List<Instruction> output = processDeserializer.deserialize(windowsInput);
		
		// Ensure that we've created 5 instructions from our five
		// input lines
		assertTrue(output.size() == 5);
	}
}
