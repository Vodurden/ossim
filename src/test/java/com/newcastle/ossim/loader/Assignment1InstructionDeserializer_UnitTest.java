package com.newcastle.ossim.loader;

import au.edu.newcastle.ossim.cpu.execution.CPUInstruction;
import au.edu.newcastle.ossim.cpu.execution.IOInstruction;
import au.edu.newcastle.ossim.cpu.execution.Instruction;
import au.edu.newcastle.ossim.loader.Assignment1InstructionDeserializer;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Assignment1InstructionDeserializer_UnitTest {

	/**
	 * Verifies that a CPU string is converted to the equivalent instruction
	 */
	@Test
	public void CPUStringIsConvertedToCPUInstruction() {
		Assignment1InstructionDeserializer deserializer = new Assignment1InstructionDeserializer();
		
		String input = "C, 1";
		
		Instruction out = deserializer.deserialize(input);
	
		assertTrue(out instanceof CPUInstruction);
	}
	
	@Test
	public void IOStringIsConvertedToIOInstruction() {
		Assignment1InstructionDeserializer deserializer = new Assignment1InstructionDeserializer();
		
		String input = "I, 5";
		
		Instruction out = deserializer.deserialize(input);
	
		assertTrue(out instanceof IOInstruction);
	}
}
